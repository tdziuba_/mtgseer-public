<?php

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(collectionOperations={"get"={"method"="GET"}, "post"={"method"="POST"}}, itemOperations={"get"={"method"="GET"}})
 * @ORM\Entity
 */
class Question
{
    /**
     * @var
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $body;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="firstQuestions")
     */
    private $firstCard;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="secondQuestions")
     */
    private $secondCard;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="thirdQuestions")
     */
    private $thirdCard;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime")
     */
    private $assignedAt;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="questions")
     */
    private $assignedBy;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Question
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Question
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set assignedAt
     *
     * @param \DateTime $assignedAt
     *
     * @return Question
     */
    public function setAssignedAt($assignedAt)
    {
        $this->assignedAt = $assignedAt;

        return $this;
    }

    /**
     * Get assignedAt
     *
     * @return \DateTime
     */
    public function getAssignedAt()
    {
        return $this->assignedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Question
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set firstCard
     *
     * @param \AppBundle\Entity\Card $firstCard
     *
     * @return Question
     */
    public function setFirstCard(\AppBundle\Entity\Card $firstCard = null)
    {
        $this->firstCard = $firstCard;

        return $this;
    }

    /**
     * Get firstCard
     *
     * @return \AppBundle\Entity\Card
     */
    public function getFirstCard()
    {
        return $this->firstCard;
    }

    /**
     * Set secondCard
     *
     * @param \AppBundle\Entity\Card $secondCard
     *
     * @return Question
     */
    public function setSecondCard(\AppBundle\Entity\Card $secondCard = null)
    {
        $this->secondCard = $secondCard;

        return $this;
    }

    /**
     * Get secondCard
     *
     * @return \AppBundle\Entity\Card
     */
    public function getSecondCard()
    {
        return $this->secondCard;
    }

    /**
     * Set thirdCard
     *
     * @param \AppBundle\Entity\Card $thirdCard
     *
     * @return Question
     */
    public function setThirdCard(\AppBundle\Entity\Card $thirdCard = null)
    {
        $this->thirdCard = $thirdCard;

        return $this;
    }

    /**
     * Get thirdCard
     *
     * @return \AppBundle\Entity\Card
     */
    public function getThirdCard()
    {
        return $this->thirdCard;
    }

    /**
     * Set assignedBy
     *
     * @param \AppBundle\Entity\User $assignedBy
     *
     * @return Question
     */
    public function setAssignedBy(\AppBundle\Entity\User $assignedBy = null)
    {
        $this->assignedBy = $assignedBy;

        return $this;
    }

    /**
     * Get assignedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getAssignedBy()
    {
        return $this->assignedBy;
    }
}
