<?php

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(collectionOperations={"get"={"method"="GET"}}, itemOperations={"get"={"method"="GET"}}, attributes={"filters"={"card.search"}})
 * @ORM\Entity
 */
class Card
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $gathererLink;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $type;
		
		/**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $multiverseid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="firstCard")
     */
    private $firstRulings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="secondCard")
     */
    private $secondRulings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="thirdCard")
     */
    private $thirdRulings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="firstCard")
     */
    private $firstQuestions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="secondCard")
     */
    private $secondQuestions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="thirdCard")
     */
    private $thirdQuestions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->firstRulings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->secondRulings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->thirdRulings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->firstQuestions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->secondQuestions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->thirdQuestions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Card
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Card
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Card
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set gathererLink
     *
     * @param string $gathererLink
     *
     * @return Card
     */
    public function setGathererLink($gathererLink)
    {
        $this->gathererLink = $gathererLink;

        return $this;
    }

    /**
     * Get gathererLink
     *
     * @return string
     */
    public function getGathererLink()
    {
        return $this->gathererLink;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Card
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add firstRuling
     *
     * @param \AppBundle\Entity\Ruling $firstRuling
     *
     * @return Card
     */
    public function addFirstRuling(\AppBundle\Entity\Ruling $firstRuling)
    {
        $this->firstRulings[] = $firstRuling;

        return $this;
    }

    /**
     * Remove firstRuling
     *
     * @param \AppBundle\Entity\Ruling $firstRuling
     */
    public function removeFirstRuling(\AppBundle\Entity\Ruling $firstRuling)
    {
        $this->firstRulings->removeElement($firstRuling);
    }

    /**
     * Get firstRulings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFirstRulings()
    {
        return $this->firstRulings;
    }

    /**
     * Add secondRuling
     *
     * @param \AppBundle\Entity\Ruling $secondRuling
     *
     * @return Card
     */
    public function addSecondRuling(\AppBundle\Entity\Ruling $secondRuling)
    {
        $this->secondRulings[] = $secondRuling;

        return $this;
    }

    /**
     * Remove secondRuling
     *
     * @param \AppBundle\Entity\Ruling $secondRuling
     */
    public function removeSecondRuling(\AppBundle\Entity\Ruling $secondRuling)
    {
        $this->secondRulings->removeElement($secondRuling);
    }

    /**
     * Get secondRulings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSecondRulings()
    {
        return $this->secondRulings;
    }

    /**
     * Add thirdRuling
     *
     * @param \AppBundle\Entity\Ruling $thirdRuling
     *
     * @return Card
     */
    public function addThirdRuling(\AppBundle\Entity\Ruling $thirdRuling)
    {
        $this->thirdRulings[] = $thirdRuling;

        return $this;
    }

    /**
     * Remove thirdRuling
     *
     * @param \AppBundle\Entity\Ruling $thirdRuling
     */
    public function removeThirdRuling(\AppBundle\Entity\Ruling $thirdRuling)
    {
        $this->thirdRulings->removeElement($thirdRuling);
    }

    /**
     * Get thirdRulings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThirdRulings()
    {
        return $this->thirdRulings;
    }

    /**
     * Add firstQuestion
     *
     * @param \AppBundle\Entity\Ruling $firstQuestion
     *
     * @return Card
     */
    public function addFirstQuestion(\AppBundle\Entity\Ruling $firstQuestion)
    {
        $this->firstQuestions[] = $firstQuestion;

        return $this;
    }

    /**
     * Remove firstQuestion
     *
     * @param \AppBundle\Entity\Ruling $firstQuestion
     */
    public function removeFirstQuestion(\AppBundle\Entity\Ruling $firstQuestion)
    {
        $this->firstQuestions->removeElement($firstQuestion);
    }

    /**
     * Get firstQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFirstQuestions()
    {
        return $this->firstQuestions;
    }

    /**
     * Add secondQuestion
     *
     * @param \AppBundle\Entity\Ruling $secondQuestion
     *
     * @return Card
     */
    public function addSecondQuestion(\AppBundle\Entity\Ruling $secondQuestion)
    {
        $this->secondQuestions[] = $secondQuestion;

        return $this;
    }

    /**
     * Remove secondQuestion
     *
     * @param \AppBundle\Entity\Ruling $secondQuestion
     */
    public function removeSecondQuestion(\AppBundle\Entity\Ruling $secondQuestion)
    {
        $this->secondQuestions->removeElement($secondQuestion);
    }

    /**
     * Get secondQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSecondQuestions()
    {
        return $this->secondQuestions;
    }

    /**
     * Add thirdQuestion
     *
     * @param \AppBundle\Entity\Ruling $thirdQuestion
     *
     * @return Card
     */
    public function addThirdQuestion(\AppBundle\Entity\Ruling $thirdQuestion)
    {
        $this->thirdQuestions[] = $thirdQuestion;

        return $this;
    }

    /**
     * Remove thirdQuestion
     *
     * @param \AppBundle\Entity\Ruling $thirdQuestion
     */
    public function removeThirdQuestion(\AppBundle\Entity\Ruling $thirdQuestion)
    {
        $this->thirdQuestions->removeElement($thirdQuestion);
    }

    /**
     * Get thirdQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThirdQuestions()
    {
        return $this->thirdQuestions;
    }

    /**
     * Set multiverseid
     *
     * @param integer $multiverseid
     *
     * @return Card
     */
    public function setMultiverseid($multiverseid)
    {
        $this->multiverseid = $multiverseid;

        return $this;
    }

    /**
     * Get multiverseid
     *
     * @return integer
     */
    public function getMultiverseid()
    {
        return $this->multiverseid;
    }
}
