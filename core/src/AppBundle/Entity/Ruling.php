<?php

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(collectionOperations={"get"={"method"="GET"}, "post"={"method"="POST"}}, itemOperations={"get"={"method"="GET"}})
 * @ORM\Entity
 */
class Ruling
{
    /**
     * @var
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $basedOn;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $text;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="firstRulings")
     */
    private $firstCard;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="secondRulings")
     */
    private $secondCard;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="thirdRulings")
     */
    private $thirdCard;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="RulingTranslation", mappedBy="ruling")
     */
    private $rulingTraslations;

    /**
     * @var string
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $verifiedAt;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="rulings")
     */
    private $verifiedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rulingTraslations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s, %s, %s', $this->firstCard, $this->secondCard, $this->thirdCard);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Ruling
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Ruling
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Ruling
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Ruling
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set basedOn
     *
     * @param string $basedOn
     *
     * @return Ruling
     */
    public function setBasedOn($basedOn)
    {
        $this->basedOn = $basedOn;

        return $this;
    }

    /**
     * Get basedOn
     *
     * @return string
     */
    public function getBasedOn()
    {
        return $this->basedOn;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Ruling
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set verifiedAt
     *
     * @param \DateTime $verifiedAt
     *
     * @return Ruling
     */
    public function setVerifiedAt($verifiedAt)
    {
        $this->verifiedAt = $verifiedAt;

        return $this;
    }

    /**
     * Get verifiedAt
     *
     * @return \DateTime
     */
    public function getVerifiedAt()
    {
        return $this->verifiedAt;
    }

    /**
     * Set firstCard
     *
     * @param \AppBundle\Entity\Card $firstCard
     *
     * @return Ruling
     */
    public function setFirstCard(\AppBundle\Entity\Card $firstCard = null)
    {
        $this->firstCard = $firstCard;

        return $this;
    }

    /**
     * Get firstCard
     *
     * @return \AppBundle\Entity\Card
     */
    public function getFirstCard()
    {
        return $this->firstCard;
    }

    /**
     * Set secondCard
     *
     * @param \AppBundle\Entity\Card $secondCard
     *
     * @return Ruling
     */
    public function setSecondCard(\AppBundle\Entity\Card $secondCard = null)
    {
        $this->secondCard = $secondCard;

        return $this;
    }

    /**
     * Get secondCard
     *
     * @return \AppBundle\Entity\Card
     */
    public function getSecondCard()
    {
        return $this->secondCard;
    }

    /**
     * Set thirdCard
     *
     * @param \AppBundle\Entity\Card $thirdCard
     *
     * @return Ruling
     */
    public function setThirdCard(\AppBundle\Entity\Card $thirdCard = null)
    {
        $this->thirdCard = $thirdCard;

        return $this;
    }

    /**
     * Get thirdCard
     *
     * @return \AppBundle\Entity\Card
     */
    public function getThirdCard()
    {
        return $this->thirdCard;
    }

    /**
     * Add rulingTraslation
     *
     * @param \AppBundle\Entity\RulingTranslation $rulingTraslation
     *
     * @return Ruling
     */
    public function addRulingTraslation(\AppBundle\Entity\RulingTranslation $rulingTraslation)
    {
        $this->rulingTraslations[] = $rulingTraslation;

        return $this;
    }

    /**
     * Remove rulingTraslation
     *
     * @param \AppBundle\Entity\RulingTranslation $rulingTraslation
     */
    public function removeRulingTraslation(\AppBundle\Entity\RulingTranslation $rulingTraslation)
    {
        $this->rulingTraslations->removeElement($rulingTraslation);
    }

    /**
     * Get rulingTraslations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRulingTraslations()
    {
        return $this->rulingTraslations;
    }

    /**
     * Set verifiedBy
     *
     * @param \AppBundle\Entity\User $verifiedBy
     *
     * @return Ruling
     */
    public function setVerifiedBy(\AppBundle\Entity\User $verifiedBy = null)
    {
        $this->verifiedBy = $verifiedBy;

        return $this;
    }

    /**
     * Get verifiedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getVerifiedBy()
    {
        return $this->verifiedBy;
    }
}
