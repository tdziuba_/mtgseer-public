<?php

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(collectionOperations={"get"={"method"="GET"}}, itemOperations={"get"={"method"="GET"}})
 * @ORM\Entity
 */
class RulingTranslation
{
    /**
     * @var
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $text;

    /**
     * @var Ruling
     *
     * @ORM\ManyToOne(targetEntity="Ruling", inversedBy="rulingTranslations")
     */
    private $ruling;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime")
     */
    private $verifiedAt;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="rulingTranslations")
     */
    private $verifiedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return RulingTranslation
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return RulingTranslation
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set verifiedAt
     *
     * @param \DateTime $verifiedAt
     *
     * @return RulingTranslation
     */
    public function setVerifiedAt($verifiedAt)
    {
        $this->verifiedAt = $verifiedAt;

        return $this;
    }

    /**
     * Get verifiedAt
     *
     * @return \DateTime
     */
    public function getVerifiedAt()
    {
        return $this->verifiedAt;
    }

    /**
     * Set ruling
     *
     * @param \AppBundle\Entity\Ruling $ruling
     *
     * @return RulingTranslation
     */
    public function setRuling(\AppBundle\Entity\Ruling $ruling = null)
    {
        $this->ruling = $ruling;

        return $this;
    }

    /**
     * Get ruling
     *
     * @return \AppBundle\Entity\Ruling
     */
    public function getRuling()
    {
        return $this->ruling;
    }

    /**
     * Set verifiedBy
     *
     * @param \AppBundle\Entity\User $verifiedBy
     *
     * @return RulingTranslation
     */
    public function setVerifiedBy(\AppBundle\Entity\User $verifiedBy = null)
    {
        $this->verifiedBy = $verifiedBy;

        return $this;
    }

    /**
     * Get verifiedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getVerifiedBy()
    {
        return $this->verifiedBy;
    }
}
