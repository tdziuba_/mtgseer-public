<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $judgeLevel;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $country;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Ruling", mappedBy="verifiedBy")
     */
    protected $rulings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="assignedBy")
     */
    protected $questions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="RulingTranslation", mappedBy="verifiedBy")
     */
    protected $rulingTranslations;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->rulings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rulingTranslations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set judgeLevel
     *
     * @param string $judgeLevel
     *
     * @return User
     */
    public function setJudgeLevel($judgeLevel)
    {
        $this->judgeLevel = $judgeLevel;

        return $this;
    }

    /**
     * Get judgeLevel
     *
     * @return string
     */
    public function getJudgeLevel()
    {
        return $this->judgeLevel;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add ruling
     *
     * @param \AppBundle\Entity\Ruling $ruling
     *
     * @return User
     */
    public function addRuling(\AppBundle\Entity\Ruling $ruling)
    {
        $this->rulings[] = $ruling;

        return $this;
    }

    /**
     * Remove ruling
     *
     * @param \AppBundle\Entity\Ruling $ruling
     */
    public function removeRuling(\AppBundle\Entity\Ruling $ruling)
    {
        $this->rulings->removeElement($ruling);
    }

    /**
     * Get rulings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRulings()
    {
        return $this->rulings;
    }

    /**
     * Add question
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return User
     */
    public function addQuestion(\AppBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Entity\Question $question
     */
    public function removeQuestion(\AppBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add rulingTranslation
     *
     * @param \AppBundle\Entity\RulingTranslation $rulingTranslation
     *
     * @return User
     */
    public function addRulingTranslation(\AppBundle\Entity\RulingTranslation $rulingTranslation)
    {
        $this->rulingTranslations[] = $rulingTranslation;

        return $this;
    }

    /**
     * Remove rulingTranslation
     *
     * @param \AppBundle\Entity\RulingTranslation $rulingTranslation
     */
    public function removeRulingTranslation(\AppBundle\Entity\RulingTranslation $rulingTranslation)
    {
        $this->rulingTranslations->removeElement($rulingTranslation);
    }

    /**
     * Get rulingTranslations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRulingTranslations()
    {
        return $this->rulingTranslations;
    }
}
