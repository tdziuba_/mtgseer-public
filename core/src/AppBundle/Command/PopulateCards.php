<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Card;
use AppBundle\Entity\Ruling;

class PopulateCards extends ContainerAwareCommand
{
    const GATHERER_LINK_PREFIX = 'http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=';
    const IMAGE_LINK_PREFIX = 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=';
    const IMAGE_LINK_SUFFIX = '&type=card';
    protected function configure()
    {
        $this->setName('app:populate:cards')
            ->setDescription('Populates cards library from MTG JSON.')
            ->setHelp('This command allows you to populate list of available cards using data from MTGJSON database');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $expansionsRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Expansion');
        $cardsRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Card');
        $expansions = $expansionsRepository->findBy(array('loaded' => false));
        foreach ($expansions as $exp)
        {
            $content = file_get_contents('http://mtgjson.com/json/'.$exp->getCode().'-x.json');
            $data = json_decode($content, true);
            $type = $data['type'];
            foreach ($data['cards'] as $element)
            {
                $checkRulings = FALSE;
                if (!isset($element['multiverseid']))
                {
                    //if card has no multiverseId we're not interested - it's not tournament legal
                    $output->writeln('Skipping card: ' . $element['name'] . '. No multiverseId provided, expansion: ' . $exp->getName());
                    continue;
                }
                $card = $cardsRepository->findOneByName($element['name']);
                if (!$card)
                {
                    $checkRulings = TRUE;
                    $output->writeln('New card - ' . $element['name'] );
                    $card = new Card();
                    $card->setName($element['name']);
                    $card->setType($element['type']);
                    if (isset($element['text']))
                        $card->setText($element['text']);
                    else
                        $card->setText('');
                }
                else
                    $output->writeln('Updating existing card - ' . $element['name']);

                $card->setImage(self::IMAGE_LINK_PREFIX . $element['multiverseid'] . self::IMAGE_LINK_SUFFIX);
                $card->setGathererLink(self::GATHERER_LINK_PREFIX . $element['multiverseid']);
                $card->setMultiverseid($element['multiverseid']);
                $em->persist($card);

                if ($checkRulings && isset($element['rulings']) && is_array($element['rulings']))
                {
                    $em->flush(); //flush is required as newly persisted Card won't have id yet otherwise
                    foreach ($element['rulings'] as $gathererRuling)
                    {
                        $ruling = new Ruling();
                        $ruling->setAuthor('Gatherer');
                        $ruling->setSource('Gatherer');
                        $ruling->setBasedOn('Gatherer');
                        $ruling->setDate(new \DateTime($gathererRuling['date']));
                        $ruling->setCreatedAt(new \DateTime($gathererRuling['date']));
                        $ruling->setVerifiedAt(new \DateTime($gathererRuling['date']));
                        $ruling->setText($gathererRuling['text']);
                        $ruling->setFirstCard($card);
                        $em->persist($ruling);
                        $em->flush();
                    }
                }


            }
            $exp->setLoaded(TRUE);
            $em->persist($exp);
            $em->flush();
        }
        $em->flush();
    }
}
