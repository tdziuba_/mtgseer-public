<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\Expansion;

class PopulateExpansions extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:populate:expansions')
            ->setDescription('Populates expansions list from MTG JSON.')
            ->setHelp('This command allows you to populate list of available expansions using data from MTGJSON database');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $content = file_get_contents('http://mtgjson.com/json/SetList.json');
        $data = json_decode($content, true);

        $repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Expansion');
        $existing = $repository->createQueryBuilder('e')->select('e.name', 'e.code','e.date as releaseDate')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $func = function(&$element)
        {
            $dateObj = $element['releaseDate'];
            $element['releaseDate'] =  $dateObj->format('Y-m-d');
            return $element;
        };
        $existing = array_map($func,$existing);
        $diff = array_map('unserialize',array_diff(array_map('serialize',$data),array_map('serialize',$existing)));

        $em = $this->getContainer()->get('doctrine')->getManager();
        $c = count($diff);
        if (!$c)
        {
            $output->writeln('Noting to update');
            exit;
        }
        foreach ($diff as $expansion)
        {


            $res = $repository->findOneByCode($expansion['code']);
            if (!$res)
            {
                $newExpansion = new Expansion();
                $newExpansion->setName($expansion['name']);
                $newExpansion->setCode($expansion['code']);
                $newExpansion->setLoaded(FALSE);
                $newExpansion->setDate(new \DateTime($expansion['releaseDate']));
                $em->persist($newExpansion);
            }
        }

        $em->flush();
        $output->writeln('Expansions updated! Count: ' . $c);
    }
}
