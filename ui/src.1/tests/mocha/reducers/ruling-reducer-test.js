/**
 * Created by Tomasz Dziuba on 12.05.2017.
 */
import reducer from '../../../app/reducers/ruling-reducer';
import * as types from '../../../app/actions/action-constants';
import { expect } from 'chai';

describe('ruling reducer', () => {

    it('should return the initial state', () => {
        expect(reducer([], {})).to.eql([]);
    });

    it('should handle RECEIVE_SINGLE_RULING action', () => {

        let ruling = {};

        expect(reducer([], { type: types.RECEIVE_SINGLE_RULING, ruling })).to.eql([
            ruling
        ]);
    });

    it('should handle REMOVE_RULING action', () => {

        let rulings = [
            {
                id: 0,
            },
            {
                id: 1,
            }
        ];

        expect(reducer(rulings, { type: types.REMOVE_RULING, rulingId: 1 })).to.eql([{id: 0}]);
    });

});
