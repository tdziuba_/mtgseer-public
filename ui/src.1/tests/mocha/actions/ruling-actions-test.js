/**
 * Created by Tomasz Dziuba on 11.05.2017.
 */
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as actions from "../../../app/actions/ruling-actions";
import * as types from "../../../app/actions/action-constants.tsx";
import { expect, assert } from "chai"; // You can use any testing library

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe("ruling actions", () => {
    it("describe receiving ruling action", () => {
        const initialState = {};
        const store = mockStore(initialState);
        const ruling = {};
        const expectedAction = { type: types.RECEIVE_SINGLE_RULING, ruling };

        store.dispatch(actions.receiveRuling(ruling));

        const acts = store.getActions();
        expect(acts).to.eql([expectedAction]);
    });

    it("describe removing ruling action", () => {
        const initialState = {};
        const store = mockStore(initialState);

        const rulingId = 121;
        const expectedAction = { type: types.REMOVE_RULING, rulingId };

        store.dispatch(actions.removeRuling(rulingId));

        const acts = store.getActions();
        expect(acts).to.eql([expectedAction]);
    });
});
