import * as types from '../actions/action-constants';
import initialState from '../initial-state';

export default function cardReducer(state = initialState.cardList, action) {
    switch(action.type) {
        case types.RECEIVE_SINGLE_CARD:

            return [
                ...state.slice(0, action.cid),
                action.card,
                ...state.slice(action.cid + 1)
            ];

        case types.REMOVE_CARD:
            let arr = [];

            state.forEach((card, index) => {
                if (index !== action.cid) {
                    arr[index] = card;
                }
            });

            return arr;

        default:
            return state;
    }
}