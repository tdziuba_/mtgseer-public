/**
 * Created by Tomasz Dziuba on 2017-06-04.
 */
import * as types from '../actions/action-constants';
import initialState from '../initial-state';

export default function suggestionReducer(state = initialState.suggestionList, action) {
    switch (action.type) {
        case types.RECEIVE_SUGGESTIONS:

            return Object.assign(state, {
                suggestions: action.suggestions.map(card => {
                    return {
                        id: card.id,
                        name: card.name,
                        image: card.image,
                        type: card.type,
                        text: card.text,
                        gathererLink: card.gathererLink,
                        rulings: [...card.firstRulings, ...card.secondRulings, ...card.thirdRulings]
                    };
                })
            });

        case types.CLEAR_SUGGESTIONS:

            return Object.assign(state, {
                suggestions: [],
                selected: null
            });

        case types.SET_SUGGESTION:

            return Object.assign(state, {
                suggestions: [],
                selected: action.selected
            });

        case types.CLEAR_SUGGESTION:

            return Object.assign(state, {
                suggestions: [],
                selected: null
            });

        default:
            return state;
    }
};
