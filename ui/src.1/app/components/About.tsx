/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-10
 * Time: 14:56
 */

import * as React from 'react';

const About = function() {

    return (
        <section className="container aboutSection">
            <h2 className="articleTitle">About</h2>
            <h3 className="subTitle">MTG Seer</h3>
            <p>MTG Seer is a project created in response for Gatherer lacking with some unintuitive rulings. It purpose is to extend Gatherer rulings by answering for uncommon questions related with particular cards and what is more important - to provide answers for interaction of up to three cards.</p>
            <p>Each ruling is checked by experienced Level 2 judge and backed up with relevant rules quotes.</p>
            <p><strong>Judges behind this project:</strong></p>
            <p>
                Bartłomiej Wieszok (L2) - Project coordinator<br/>
                Sebastian Fyda (L1) - Originator & application developer<br/>
                Sergiy Pastukhov (L2) - Content moderator
                <br/>
            </p>
        </section>
    );

};

export default About;