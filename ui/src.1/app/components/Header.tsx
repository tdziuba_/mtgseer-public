/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2016-12-25
 * Time: 13:13
 */

import * as React from 'react';
import { Component, ReactHTMLElement } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component<{},{}> {

    state = {
        isMenuOpen: false
    };

    links: any;

    onMenuButtonClick(): void {
        this.links.className = this.getLinksClassName();
    }

    getLinksClassName(): string {
        let cssClass = this.links.className;

        if (cssClass.indexOf('mobile-hide tablet-hide') > -1) {
            return cssClass.replace('mobile-hide tablet-hide', '');
        } else {
            return cssClass + ' mobile-hide tablet-hide';
        }

    }

    render(): JSX.Element {

        return (
            <div className="SL-header container">
                <Link to="/" className="logo headerLink">
                    <h1 className="title">
                        <img className="responsive"
                             src="/assets/img/logo.png"
                             srcSet="/assets/img/logo@2x.png 2x, /assets/img/logo@3x.png 2x"
                             alt="MTG Seer"/>
                        <span className="sr-only">MTG Seer</span>
                        <small className="subTitle mobile-sr">  comprehensive MTG Ruling database</small></h1>
                </Link>
                <nav ref={(links) => { this.links = links }}
                     className="headerLinks mobile-hide tablet-hide"
                >
                    <Link to="/about" className="headerLink navLink" activeClassName="active"><span className="navLinkText">About</span></Link>
                    <Link to="/submit-question" className="headerLink navLink" activeClassName="active"><span className="navLinkText">Submit a question</span></Link>
                    <Link to="/new-ruling" className="headerLink navLink" activeClassName="active"><span className="navLinkText">Submit new Ruling</span></Link>
                </nav>
                <button className="btn mobileMenuBtn desktop-hide large-hide"
                        ref={(menuBtn) => { this.menuBtn = menuBtn }}
                        onClick={this.onMenuButtonClick.bind(this)}
                >
                    <i className="fa fa-bars" aria-hidden="true" />
                </button>
            </div>
        );
    }
}

export default Header;
