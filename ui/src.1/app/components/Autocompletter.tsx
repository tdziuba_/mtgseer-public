/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-20
 * Time: 21:07
 */

// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearAutosugesstions, setSingleAutosugesstion } from '../actions/suggestion-actions';
import { fetchAutosuggestions } from '../actions/action-creators';

class Autocompletter extends Component {

    input: HTMLInputElement;
    autocompletter: Component;

    constructor (props: any) {
        super(props);

        this.state = {
            isLoading: false,
            selected: null
        };
    }

    componentDidUpdate () {
        let { suggestionList } = this.props;

        if (!suggestionList.length || !this.autocompletter) {
            return;
        }

        let top = this.autocompletter.scrollTop;

        document.body.scrollTop = top - 50;

    }

    componentWillUnmount () {
        this.clearSuggestions();
    }

    onChange (ev) {
        let { dispatch } = this.props;
        let suggestion = JSON.parse(ev.target.value);

        this.clearSuggestions();

        this.setState({
            selected: ev.target.value,
            isLoading: false
        });

        this.input.value = suggestion.name;

        dispatch( setSingleAutosugesstion(suggestion) );

    }

    onKeyUp (ev: Event) {
        ev.persist();
        clearTimeout(this.t);

        const ESC_KEY: number = 27;
        const CARD_NAME: string = this.state.typedCardName;
        const TYPED_CARD_NAME: string = ev.currentTarget.value;


        if (ev.target.value.length < 3) {
            return;
        }

        if (ev.keyCode === ESC_KEY) {
            this.onClose();

            return;
        }

        this.t = setTimeout(() => {

            if (CARD_NAME ===  this.state.typedCardName) {
                return this.getAutocompletterSuggestions(ev);
            }

            this.setState({ typedCardName: TYPED_CARD_NAME });
        }, 500);

    }

    getAutocompletterSuggestions(ev: Event) {
        let { dispatch } = this.props,
            name: string = ev.target.value;

        try {

            let callback = () => {
                this.setState({
                    isLoading: false
                })
            };

            this.setState({
                isLoading: true
            });

            dispatch( fetchAutosuggestions( name, callback ) );

        } catch (err) {
            Logger.warn('getautocompletterSuggestions problem', err);
        }

    }

    clearSuggestions () {
        let { dispatch } = this.props;

        this.setState({ selected: null, isLoading: false });

        dispatch(clearAutosugesstions());

    }

    renderList () {

        let { suggestionList } = this.props;

        if (!suggestionList.suggestions.length) {
            return;
        }

        let counter = 0,
            items = suggestionList.suggestions.map((item) => {
                counter++;
                return this.renderItem(counter, item);
            });

        return (
            <ul className="autocompleter-list">{ items }</ul>
        );
    }

    renderItem (index, item) {

        return (
            <li key={index}
                className="autocompleter-item"
                data-id={ item.id }
            >
                <label className="suggestionLabel">
                    <input type="radio"
                           //ref={ (input) => { this.input = input; } }
                           className="radio-hidden"
                           name="name"
                           autoFocus="autoFocus"
                           value={ JSON.stringify(item) }
                           onChange={ (ev) => { this.onChange(ev) } }
                    />
                    <span className="suggestion">{ item.name }</span>
                </label>
            </li>
        );
    }

    renderSpinner () {
        return (this.state.isLoading) ? <i className="fa fa-spinner fa-spin" aria-hidden="true" /> : null;
    }

    render () {
        let { inputProps } = this.props;

        return (
            <div ref={ (autocompletter) => { this.autocompletter = autocompletter }} className="autocompleter">

                { this.renderSpinner() }

                <input type="text"
                       ref={ (input) => { this.input = input } }
                       {...inputProps}
                       onKeyUp={ (ev) => this.onKeyUp(ev) }
                />
                { this.renderList() }

            </div>
        );
    }
}

function mapStateToProps(state) {
    return { suggestionList: state.suggestionList }
}

export default connect(mapStateToProps, null, null, { withRef: true })(Autocompletter);

//
// export default connect((state) => ({
//     suggestionList: state.suggestionList
// }, null, null, {withRef: true}))(Autocompletter);
