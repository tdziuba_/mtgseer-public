/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-20
 * Time: 07:32
 */

import * as React from 'react';
import { Component, ReactHTML, ReactNode} from 'react'

class Button extends Component<any |{}> {
    state = {
        isLoading: false,
        isDisabled: false
    };

    onClick () {

        let { callback: Function } = this.props;

        callback();
    }

    setButtonInLoadingState () {
        this.setState({isLoading: true, isDisabled: true});

        return this;
    }

    setButtonInReadyState (): Button {
        this.setState({isLoading: false, isDisabled: true});
        this.disableButton();
        this.refs.button.setAttribute('disabled', 'disabled');

        return this;
    }

    disableButton (): Button {
        this.setState({isDisabled: true});

        return this;
    }

    enableButton (): Button {
        this.setState({isDisabled: false});

        return this;
    }

    getCssClass (): string {
        let { cssClass } = this.props;

        return (this.state.isLoading) ? cssClass + ' loading' : cssClass;
    }

    render (): ReactNode {
        let { children } = this.props;

        return (
            <button className={ this.getCssClass() }
                    onClick={ () => { this.onClick() } }
                    disabled={ this.state.isDisabled ? 'disabled' : '' }
            >
                <i className={(this.state.isLoading) ? 'fa fa-spin fa-circle-o-notch' : '' }/>
                <strong className="btnText">{ children }</strong>
            </button>
        )

    }
}

export default Button;
