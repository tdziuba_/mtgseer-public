/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-11
 * Time: 16:25
 */

// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getScrollbarWidth } from '../helpers/general';
import Button from './Button';
import Autocompletter from './Autocompletter';
import { getCardData } from '../actions/action-creators';
import Logger from '../helpers/Logger';

class CardForm extends Component<any> {

    submitBtn: Button<any>;
    autocompletter: Component<any>;
    parent: Component<any>;
    input: Element;
    t;

    state: {
        value: string,
        cards: Array,
        loading: boolean,
        submitDisabled: boolean,
        isVisible: boolean,
        typedCardName: string
    };

    constructor(props: any) {
        super(props);

        this.parent = this.props.parent;

        this.state = {
            value: '',
            cards: [],
            loading: false,
            submitDisabled: true,
            isVisible: false,
            typedCardName: ''
        }
    }

    componentDidUpdate() {

        let root = document.getElementsByTagName('body')[0];

        if (this.state.isVisible) {
            root.className += ' no-scroll';
            document.body.style.cssText = 'padding-right: ' + getScrollbarWidth() + 'px';
        } else {
            root.className = root.className.replace(/no\-scroll/g, '').trim();
            document.body.style.paddingRight = '';
        }
    }

    componentWillUnmount() {

        let root = document.getElementsByTagName('body')[0];
        root.className = root.className.replace(/no\-scroll/g, '').trim();

        document.body.style.paddingRight = '';
    }

    render() {

        return (this.state.isVisible) ? this.renderCardForm() : null;
    }

    renderCardForm () {

        const { suggestionList } = this.props;
        const inputProps: Object = {
            className: "input",
            autoFocus: "autofocus",
            placeholder: "Start typing...",
            autoCorrect: "off",
            autoComplete: "off",
            value: suggestionList.selected ? suggestionList.selected.name : undefined
        };

        return (
            <div className="popup cardFormPopup">
                <div className="popupBox">
                    <button className="btn closeBtn"
                            onClick={ () => this.onClose() }
                    >
                        <i className="fa fa-times" aria-hidden="true"/>
                    </button>
                    <h3 className="popupHeader">Find a card to add to the ruling</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
                    <div className="SL-form cardForm">

                        <label className="form-label cardFormLabel">
                            <strong className="labelText">
                                <span className="placeholder-text">Card name</span>
                            </strong>

                            <Autocompletter ref={ (autocompletter) => { this.autocompletter = autocompletter } }
                                           inputProps={ inputProps }
                            />
                        </label>

                        <label className="cardFormLabel clearfix">
                            <Button ref={ (btn) => { this.submitBtn = btn; }}
                                    parent={ this }
                                    cssClass="btn btn-secondary btn-submit submitCardBtn"
                                    callback={ () => this.onSubmit() }
                            >
                                Add card
                            </Button>
                        </label>
                    </div>

                </div>
                <div className="popupLayer" onClick={ () => this.onClose() } />
            </div>
        );

    }

    onClose() {

        try {
            this.setState({ isVisible: false });
        } catch (err) {
            Logger.warn('onClose problem', err);
        }

    }


    /**
     * set isVisible to show or hide popup
     * @param {bool} visibility
     */
    setPopupVisibility(visibility: boolean) {
        this.setState({ isVisible: visibility });

        return this;
    }

    onSubmit() {

        try {

            let { dispatch, suggestionList, cid } = this.props;

            this.submitBtn.setButtonInLoadingState();

            dispatch( getCardData( suggestionList.selected, cid ) )


        } catch (err) {
            Logger.warn('CardForm onSubmit problem', err);
        }
    }

    onButtonClick(ev: Event) {
        this.onSubmit(ev);
    }


}

function mapStateToProps (state) {
    return {
        cardList: state.cardList,
        suggestionList: state.suggestionList
    }
}

export default connect(mapStateToProps, null, null, { withRef: true })(CardForm);
