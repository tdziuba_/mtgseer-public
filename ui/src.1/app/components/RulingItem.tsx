/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-04-02
 * Time: 11:05
 */

import React, { Component } from 'react';

export default class RulingItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        let { ruling, headerText } = this.props;

        return (
            <div className="rulingItem">

                { this.renderHeader(headerText) }

                <p>{ ruling.text }</p>
                <p className="basedOn"><strong>Based on: { ruling.basedOn }</strong> by { ruling.author }</p>
            </div>
        );
    }

    renderHeader (headerText) {

        if (headerText === '') {
            return;
        }

        return (
            <h3>{ headerText }</h3>
        );

    }
}
