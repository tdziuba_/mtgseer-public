/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-19
 * Time: 08:00
 */

import React, { Component } from 'react';
import { addEvent, removeEvent, preventEvent } from '../helpers/CustomHandler';
import Button from './Button';
import Dialog from './Dialog';

class NewRulingForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            author: '',
            source: '',
            basedOn: '',
            description: '',
            errorMsg: ''
        };

        this.parent = this.props.parent;
    }

    componentDidMount () {
        addEvent(this.refs.authorInput, 'keyup', this.updateAuthor.bind(this));
        addEvent(this.refs.sourceInput, 'keyup', this.updateSource.bind(this));
        addEvent(this.refs.basedOnInput, 'keyup', this.updateBasedOn.bind(this));
        addEvent(this.refs.rulingDescriptionInput, 'keyup', this.updateRulingDescription.bind(this));
        addEvent(this.refs.form, 'submit', this.onSubmit.bind(this));
    }

    componentWillUnmount () {
        removeEvent(this.refs.authorInput, 'keyup', this.updateAuthor);
        removeEvent(this.refs.sourceInput, 'keyup', this.updateSource);
        removeEvent(this.refs.basedOnInput, 'keyup', this.updateBasedOn);
        removeEvent(this.refs.rulingDescriptionInput, 'keyup', this.updateRulingDescription);
        removeEvent(this.refs.form, 'submit', this.onSubmit);
    }

    updateAuthor () {
        this.setState({
            author: this.refs.authorInput.value
        });
    }

    updateSource () {
        this.setState({
            source: this.refs.sourceInput.value
        });
    }

    updateBasedOn () {
        this.setState({
            basedOn: this.refs.basedOnInput.value
        });
    }

    updateRulingDescription () {
        this.setState({
            description: this.refs.rulingDescriptionInput.value
        });
    }

    render() {

        return (
            <form className="SL-form newRulingForm">
                <label className="form-label RulingFormLabel">
                    <strong className="labelText">
                        <i className="fa fa-user-circle-o" aria-hidden="true" />
                        <span className="placeholder-text">Author</span>
                    </strong>
                    <input type="text" id="author" ref="authorInput" name="author" className="input" placeholder="Author" />
                </label>

                <label className="form-label RulingFormLabel">
                    <strong className="labelText">
                        <i className="fa fa-file" aria-hidden="true" />
                        <span className="placeholder-text">Source</span>
                    </strong>
                    <input type="text" id="source" ref="sourceInput" name="source" className="input" placeholder="Source" />
                </label>

                <label className="form-label RulingFormLabel">
                    <strong className="labelText">
                        <i className="fa fa-home" aria-hidden="true" />
                        <span className="placeholder-text">Based on</span>
                    </strong>
                    <input type="text" id="based_on" ref="basedOnInput" name="based_on" className="input" placeholder="Based on" />
                </label>

                <label className="form-label RulingFormLabel">
                    <strong className="labelText">
                        <span className="placeholder-text">Ruling description</span>
                    </strong>
                    <textarea type="text" id="ruling" ref="rulingDescriptionInput" name="ruling" className="input" placeholder="Ruling description" />
                </label>

                <label className="form-label clearfix">
                    <Button ref="submitBtn"
                            cssClass="btn btn-primary btn-submit submitRulingBtn"
                            parent={this}>Submit new Ruling</Button>
                </label>

                <Dialog parent={this} ref="dialog" />

            </form>
        );
    }

    enableButton () {
        this.refs.submitBtn.enableButton().setButtonInReadyState();
    }

    onButtonClick (ev) {
        preventEvent(ev);

        this.refs.submitBtn.setButtonInLoadingState().disableButton();

        this.onSubmit();
    }

    onSubmit (ev) {
        preventEvent(ev);

        let { state } = this.props;

        if ( this.isFormValid() ) {
            this.refs.submitBtn.setButtonInLoadingState();

        } else {
            this.refs.submitBtn.setButtonInReadyState();

            this.refs.dialog.setUpDialog({
                title: 'Warning!',
                message: this.state.errorMsg.toString()
            }).show();

        }

        let data = {
            author: this.state.author,
            source: this.state.source,
            text: this.state.description,
            basedOn: this.state.basedOn,
            firstCard: (state.cardList && typeof state.cardList[0] !== 'undefined') ? state.cardList[0] : '',
            secondCard: (state.cardList && typeof state.cardList[1] !== 'undefined') ? state.cardList[1] : '',
            thirdCard: (state.cardList && typeof state.cardList[2] !== 'undefined') ? state.cardList[2] : ''
        };

        this.parent.onRulingFormSubmit(data);
    }

    isFormValid () {
        let { state } = this.props;
        let fieldsFilled = !(this.state.author == '' || this.state.description == '' || this.state.source == '' || this.state.basedOn == '');

        if (!fieldsFilled) {

            if (state.cardList.length >= 2) {

                this.setState({
                    errorMsg: 'All fields are required!'
                });

            } else {

                this.setState({
                    errorMsg: 'Select minimum 2 cards to submit a question'
                });

            }

            this.refs.submitBtn.disableButton();

            return false;

        } else if (state.cardList.length < 2 && fieldsFilled) {
            this.setState({
                errorMsg: 'Select minimum 2 cards to submit a question'
            });

            this.refs.submitBtn.disableButton();

            return false;
        } else {
            this.setState({
                errorMsg: ''
            });

            this.refs.submitBtn.setButtonInReadyState();

            return true;
        }
    }

    resetForm () {
        this.setState({
            author: '',
            source: '',
            basedOn: '',
            description: '',
            errorMsg: ''
        });

        this.refs.dialog.close();
    }

    showConfirmation () {
        this.resetForm();

        let self = this;

        this.refs.dialog.setUpDialog({
            message: 'Your ruling was submit',
            title: 'Thank you!'
        }).show();

        setTimeout(() => {
            self.refs.dialog.close();
        }, 2000);
    }

}

export default NewRulingForm;
