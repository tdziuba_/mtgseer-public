// Rulings
export const ADD_RULING: string                         = 'ADD_RULING';
export const REMOVE_RULING: string                      = 'REMOVE_RULING';
export const UPDATE_RULING: string                      = 'UPDATE_RULING';
export const GET_SINGLE_RULING: string                  = 'GET_SINGLE_RULING';
export const RECEIVE_SINGLE_RULING: string              = 'RECEIVE_SINGLE_RULING';
export const GET_RULING_LIST: string                    = 'GET_RULING_LIST';
export const RECEIVE_RULING_LIST: string                = 'RECEIVE_RULING_LIST';

// cards
export const ADD_CARD: string                           = 'ADD_CARD';
export const REMOVE_CARD: string                        = 'REMOVE_CARD';
export const UPDATE_CARD: string                        = 'UPDATE_CARD';
export const GET_SINGLE_CARD: string                    = 'GET_SINGLE_CARD';
export const RECEIVE_SINGLE_CARD: string                = 'RECEIVE_SINGLE_CARD';
export const GET_CARD_LIST: string                      = 'GET_CARD_LIST';
export const RECEIVE_CARD_LIST: string                  = 'RECEIVE_CARD_LIST';
export const UPDATE_RULING_LIST_ON_CARD_REMOVE: string  = 'UPDATE_RULING_LIST_ON_CARD_REMOVE';


// autosuggestion
export const RECEIVE_SUGGESTIONS: string                = 'RECEIVE_SUGGESTIONS';
export const CLEAR_SUGGESTIONS: string                  = 'CLEAR_SUGGESTIONS';
export const SET_SUGGESTION: string                     = 'SET_SUGGESTIONS';
export const CLEAR_SUGGESTION: string                   = 'CLEAR_SUGGESTION';
