/**
 * Created by Tomasz Dziuba on 2017-06-04.
 */
import * as types from './action-constants';

export function receiveAutosugesstions (suggestions) {
    return { type: types.RECEIVE_SUGGESTIONS, suggestions };
}

export function clearAutosugesstions () {
    return { type: types.CLEAR_SUGGESTIONS, suggestions: [] };
}

export function setSingleAutosugesstion (suggestion) {
    return { type: types.SET_SUGGESTION, selected: suggestion };
}

export function clearSingleAutosugesstions () {
    return { type: types.CLEAR_SUGGESTION, selected: null };
}