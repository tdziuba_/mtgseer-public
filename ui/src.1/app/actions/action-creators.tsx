/**
 * Created by Tomasz Dziuba on 2017-05-28.
 */
 
import * as types from './action-constants';
import { receiveCard, removeCard } from './card-actions';
import { receiveAutosugesstions, clearAutosugesstions } from './suggestion-actions';
import { receiveRuling, removeRuling } from './ruling-actions';
import { CardsApi } from '../api';
import Logger from '../helpers/Logger';

export function fetchAutosuggestions(text, callback) {
    return (dispatch, getState) => {

        CardsApi.fetchAutosuggestions(text, callback, (err) => Logger.warn('Something wrong with getting card name', err))
        // try {

        //     axios.get(`${ window.__PRELOADED_STATE__.config.API_URI }/cards?name=${ text }`)
        //         .then(response => {

        //             if (response.status === 200 && response.statusText === 'OK') {
        //                 dispatch( receiveAutosugesstions(response.data['hydra:member']) );

        //                 return callback();
        //             }

        //         })
        //         .catch(function (error) {
        //             Logger.warn('Something wrong with getting card name', error);
        //         });

        // } catch (err) {
        //     Logger.warn('Something wrong with getting card name', err);
        // }

    };
}

export function fetchRulingList (rulingAddresses, callback) {
    return (dispatch, getState) => {
        rulingAddresses.forEach(address => {
            axios.get(window.__PRELOADED_STATE__.config.API_URI + address)
                .then(response => {
                    if (response.status === 200 && response.statusText === 'OK') {
                        dispatch(receiveRuling(response.data));

                        return (callback) ? callback() : null;
                    }
                })
                .catch(err => {
                    console.debug(err);
                    throw new Error(err);

                });
        });
    };
}

export function getCardData (card, cid) {
    return (dispatch, getState) => {
        dispatch( receiveCard(card, cid) );
        dispatch( fetchRulingList(card.rulings) )
    };
}
