import * as types from './action-constants';

export function receiveCard (card, cid) {
    return { type: types.RECEIVE_SINGLE_CARD, card, cid };
}

export function removeCard (card, cid) {
    return { type: types.REMOVE_CARD, card, cid };
}
