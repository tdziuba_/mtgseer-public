import * as types from './action-constants';

export function receiveRulingList (rulings) {
    return { type: types.RECEIVE_RULING_LIST, rulings }
}

export function receiveRuling (ruling) {
    return { type: types.RECEIVE_SINGLE_RULING, ruling };
}

export function removeRuling (rulingId) {
    return { type: types.REMOVE_RULING, rulingId };
}