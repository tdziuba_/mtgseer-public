import * as React from 'react';
import { Component, ReactPropTypes } from 'react';
import { connect, ProviderProps } from 'react-redux';
import axios from 'axios';
import NewRulingForm from '../components/NewRulingForm';
import RulingList from '../components/RulingList';
import Logger from '../helpers/Logger';
import { StateEntity } from '../entities'
import { RulingEntity } from '../entities/ruling-entities';

interface SubmitRulingInternalState {
    isLoading: boolean;
    rulingList: Array<RulingEntity>
}

class SubmitRuling extends Component<ReactPropTypes, {}> {
    constructor(props: ReactPropTypes) {
        super(props);

        this.state = {
            isLoading: false,
            rulingList: []
        } as SubmitRulingInternalState;
    }

    componentDidMount () {
        
    }

    render () {
        let { children, state } = this.props;

        return (
            <section className="commonSection submitNewRuling">
                <RulingList ref="ruling" parent={this} legendText="Select cards to submit new ruling" />
                <article className="article submitNewRulingArticle">
                    <div className="container">
                        <h2 className="articleTitle">Cards Rulings</h2>
                        <h3 className="subTitle">Submit new Ruling</h3>
                        <p>If you don’t know the ruling to this combination of cards you can simply submit your question by filling up the form.</p>
                        <NewRulingForm ref="form" parent={this} state={state} />
                    </div>
                </article>
            </section>
        );
    }

    onRulingListChange () {
        let { state: StateEntity } = this.props;

        try {

            this.setState({
                isLoading: false,
                rulingList: state.rulingList
            });

        } catch (err) {
            Logger.warn('onRulingListChange problem', err);
        }

        return this;
    }

    onRulingFormSubmit (data) {
        let { state } = this.props;
        const { form: NewRulingForm } = this.refs;

        if (state.cardList.length) {
            if (data) {

                axios.post(process.env.CONF.API_URI + '/rulings', data)
                    .then(function (response) {
                        console.log(response);

                        form.enableButton();
                        form.resetForm();
                    })
                    .catch(function (err) {
                        self.refs.form.enableButton();

                        Logger.warn('Something wrong with submiting question to API', err);
                    });

            }
        }
    }

}

const mapStateToProps = (state: StateEntity) => ({
    state: state
});

export default connect(mapStateToProps)(SubmitRuling);
