import React, { Component } from 'react';
import { connect, ProviderProps } from 'react-redux';
import Logger from '../helpers/Logger';
import RulingSet from './RulingSet';
import RulingList from '../components/RulingList';

class HowItWorks extends Component {
    constructor(props) {
        super(props);
    }

    render () {

        return (
            <section className="commonSection howItWorks">

                <RulingSet ref="ruling" legendText="Select cards to get rulings" />

                { this.renderBottom() }

            </section>
        );
    }

    renderBottom () {
        let { cardList, rulingList } = this.props;

        return this.showRulingList(cardList) ?
            <RulingList ref={(list) => { this.rulingList = list }}
                        cardList={ cardList }
                        rulingList={ rulingList }
            /> : this.renderArticle();
    }

    renderArticle () {

        return (
            <article className="article howItWorksArticle">
                <div className="container">
                    <h2 className="articleTitle">How it works</h2>
                    <h3 className="brandName">MTG Seer</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium pharetra nisl quis
                        eleifend.
                        Fusce dictum risus tempus nisl pellentesque, semper porttitor nulla iaculis. In ac rutrum erat.
                        Nulla posuere neque id nibh gravida, sed feugiat orci accumsan. Vestibulum ultrices convallis
                        magna sit amet mollis. Ut pharetra mauris ac bibendum consequat.
                        Maecenas euismod vestibulum congue.</p>
                    <p>In bibendum aliquet mauris, vitae facilisis magna molestie quis. Sed non tellus suscipit urna
                        convallis tincidunt. Vivamus quis augue at velit placerat ornare. Vestibulum ultricies mauris
                        pretium lorem suscipit, id rutrum sapien rhoncus. Sed id ultricies risus. Donec sagittis sit
                        amet
                        lacus vel efficitur. </p>
                    <p>Integer auctor felis erat, sit amet sollicitudin sem ullamcorper id. Aenean at tellus vel neque
                        sollicitudin consectetur. Etiam at massa ultrices dolor aliquam lobortis. Nulla aliquam nunc
                        at mi iaculis, at mollis risus pulvinar. Phasellus sodales non nibh vehicula pellentesque.
                        Phasellus mi augue, fringilla nec ullamcorper ut, pharetra ut tortor.</p>
                </div>
            </article>
        );

    }

    showRulingList (cardList) {

        let nonEmpty = cardList.filter((e) => {
            return ( typeof  e !=='undefined' && e !== null && e !== '');
        });

        if (!nonEmpty.length) {
            return false;
        }

        return true;
        //
        // let card_ids = cardList.map((card) => {
        //     if (card !== null) {
        //         return card.id;
        //     }
        // });
        //
        // let ruling_cards = cardList.filter((card) => {
        //     return ( card !== null && card_ids.includes(card.id) );
        // });
        //
        // return (card_ids.length === ruling_cards.length);
    }

    onRulingListChange () {
        let { rulingList } = this.props;

        try {

            this.setState({
                isLoading: false,
                rulingList: rulingList
            });

        } catch (err) {
            Logger.warn('onRulingListChange problem', err);
        }

        return this;
    }

}

const mapStateToProps = (state) => ({
    cardList: state.cardList,
    rulingList: state.rulingList
});

export default connect(mapStateToProps)(HowItWorks);
