import * as React from 'react';
import { Component, ReactPropTypes } from 'react';
import { connect, ProviderProps } from 'react-redux';
import axios from 'axios';
import RulingSet from './RulingSet';
import RulingList from '../components/RulingList';
import QuestionForm from '../components/QuestionForm';
import Logger from '../helpers/Logger';
import StateEntity from '../entities'

class SubmitQuestion extends Component<ReactPropTypes, {}> {
    constructor(props: ReactPropTypes) {
        super(props);
    }

    render () {
        let { children, state: StateEntity } = this.props;

        return (
            <section className="commonSection submitQuestion">
                <RulingSet ref="ruling" legendText="Select cards to get rulings" />
                <article className="article submitQuestionArticle">
                    <div className="container">
                        <h2 className="articleTitle">Cards rulings</h2>
                        <h3 className="subTitle">Submit a question to this ruling</h3>
                        <p>If you don’t know the ruling to this combination of cards you can simply submit your question by filling up the form.</p>
                        <QuestionForm ref="form" parent={this} state={state} />
                    </div>
                </article>
            </section>
        );
    }

    onRulingListChange () {
        let { state: StateEntity } = this.props;

        try {

            this.setState({
                isLoading: false,
                rulingList: state.rulingList
            });

        } catch (err) {
            Logger.warn('onRulingListChange problem', err);
        }

        return this;
    }

    onFormSubmit (data) {
        let { state } = this.props;
        let self = this;

        if (!state.cardList.length) {
            alert('Select card(s) first!');
        } else {
            if (data) {

                axios.post(window.__PRELOADED_STATE__.config.API_URI + '/questions', data)
                    .then(function (response) {
                        console.log(response);

                        self.refs.form.enableButton();
                    })
                    .catch(function (err) {
                        self.refs.form.enableButton();

                        Logger.warn('Something wrong with submiting question to API', err);
                    });

            }
        }
    }

}

const mapStateToProps = (state: StateEntity) => ({
    state: state
});

export default connect(mapStateToProps)(SubmitQuestion);
