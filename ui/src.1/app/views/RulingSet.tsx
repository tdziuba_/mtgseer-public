import React, { Component } from 'react';
import { connect, ProviderProps } from 'react-redux';
import axios from 'axios';
import Logger from '../helpers/Logger';
import { receiveRuling } from '../actions/ruling-actions';
import Card from '../components/Card';
import Legend from '../components/Legend';

class RulingSet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cards: [],
            rulings: []
        };

        this.parent = this.props.parent;
    }

    render () {
        let { children } = this.props;

        return (
            <div className="SL-ruling-form container">
                <div className="addRulingForm">

                    { this.renderFormCaption() }

                    <div className="rulingList">
                        { this.renderCards() }
                    </div>
                </div>
                { children }
            </div>
        );
    }

    renderFormCaption () {
        let { state, legendText } = this.props;

        return ( !this.showLegend(state) ) ? null : <Legend>{ legendText }</Legend>;
    }

    renderCards () {
        let cards = [], i = 0;
        let { dispatch, state } = this.props;

        for (i; i < 3; ++i) {
            cards.push(
                <Card key={ i } cid={ i } parent={ this } />
            );
        }

        return cards;
    }

    showLegend (state) {
        let show = false;

        if (state.cardList.length) {
            show = state.cardList.some((el) => {
                return el !== null;
            });
        }

        return !show;
    }

    setCard (card, cid) {
        let self = this;
        let cards = this.state.cards;
        let { dispatch, state } = this.props;

        try {
            cards[cid] = card;

            self.setState({
                hideLabel: true,
                cards: cards
            });

            self.onCardUpdate(card);

        } catch (err) {
            Logger.warn('set card state problem', err);
        }

        return this;
    }

    onCardUpdate (card) {
        let self = this;

        if (card.firstRulings.length) {
            for (let idx in card.firstRulings) {

                self.getRuling(card.firstRulings[idx]);

            }
        }

        return this;
    }

    getRuling (url) {
        let self = this;
        let { dispatch } = this.props;

        axios.get(window.__PRELOADED_STATE__.config.API_URI + url).then(response => {

            try {
                dispatch(receiveRuling(response.data));

                self.parent.onRulingListChange();
            } catch (err) {
                Logger.warn('setting up ruling problem', err);
            }
        });
    }

}

const mapStateToProps = (state) => ({
    state: state
});

export default connect(mapStateToProps)(RulingSet);
