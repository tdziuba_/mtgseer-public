import * as React from 'react';
import { Component, ReactPropTypes } from 'react';
import { connect, ProviderProps } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/ruling-actions';
import Header from '../components/Header';
import Logger from '../helpers/Logger';

class App extends Component<{}, {}> {
    constructor(props: ProviderProps) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error: Error, info: string) {
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        Logger.warn(info, error);
    }

    render () {
        let { children, state } = this.props;

        return (
            <div className="SL-app">
                <Header state= {state} parent={ this } />
                <section className="SL-content">
                    { children }
                </section>
                <footer className="SL-footer"><p>Copyrights &copy; { this.getYear() } by MTG Seer </p></footer>
            </div>
        );
    }

    getYear() {
        let date = new Date();

        return date.getFullYear();
    }

}

const mapStateToProps = (state) => ({
    state: state
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
