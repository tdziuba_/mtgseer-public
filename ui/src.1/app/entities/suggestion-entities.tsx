interface SuggestionEntity {

}

interface SuggestionListEntity {
    suggestions: Array<SuggestionEntity>,
    selected: SuggestionEntity
}

export {
    SuggestionListEntity,
    SuggestionEntity
}