import * as CardEntity from './card-entities'
import * as RulingEntity from './ruling-entities'
import {SuggestionListEntity, SuggestionEntity} from './suggestion-entities'

interface StateEntity {
    cardList: Array<CardEntity>,
    rulingList: Array<RulingEntity>,
    suggestions: SuggestionListEntity
}

export default {
    CardEntity,
    RulingEntity,
    SuggestionListEntity,
    StateEntity,
}