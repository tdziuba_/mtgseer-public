import axios from 'axios';
import Logger from '../helpers/Logger';

function apiCall(url, options, callback, onErrorCallback) {
    return axios
            .get(url, options = {})
            .then((response) => {
                if (response.status === 200 && response.statusText === 'OK') {
                    callback(response)
                }
            })
            .catch(err => onErrorCallback(err));
}

const fetchAutosuggestions = (text, callback, onError) => {
    const url = `${ window.__PRELOADED_STATE__.config.API_URI }/cards?name=${ text }`;
        
    apiCall(url, {}, callback, onError)
}

export {
    fetchAutosuggestions
}