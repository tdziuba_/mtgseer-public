import * as React from 'react';
import { ReactElement } from 'react';
import { hydrate } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import * as storage from 'redux-storage'
import createEngine from 'redux-storage-engine-localstorage';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router-dom';
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers';
import routes from './routes';
import defaultState from './initial-state';
import * as styles from '../scss/index.scss';

declare var window: Window;
declare var document: Document;

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        name: 'MTGSeer', actionsBlacklist: ['REDUX_STORAGE_SAVE']
    }) : compose;

//const initialState = window.__PRELOADED_STATE__ || defaultState;
const initialState = defaultState;


const reducer = storage.reducer(rootReducer);
const engine = createEngine('mtgseer');
const storageMiddleware = storage.createMiddleware(engine);

const middleware: Array<any> = [ ReduxThunk, storageMiddleware ];

export const store = createStore(
    reducer,
    initialState,
    composeEnhancers(
        applyMiddleware(...middleware),
        // other store enhancers if any
    )
);

const load = storage.createLoader(engine);

if (typeof localStorage !== 'undefined') {
    load(store);
}

export const App = () => (<Provider store={store}>
<Router history={browserHistory} store={store}>{routes}</Router>
</Provider>) as ReactElement<any>;

if (typeof window !== 'undefined') {
    hydrate(
        App(),
        document.getElementById('root')
    );
}