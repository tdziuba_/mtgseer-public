
import _ from 'underscore';
import Logger from './Logger';

class RulingListHelper {

    constructor () {
        return {
            showRuling: this.showRuling,
            getRulingCardsIds: this.getRulingCardsIds,
            getFirstCardId: this.getFirstCardId,
            getSecondCardId: this.getSecondCardId,
            getThirdCardId: this.getThirdCardId,
            getCardId: this.getCardId,
            compareIds: this.compareIds,
            getRulingHeader: this.getRulingHeader,
            howManyTimes: this.howManyTimes
        }
    }

    getCardId (url) {
        return parseInt( url.replace('/cards/', '') );
    }

    getFirstCardId (ruling) {
        return (ruling.firstCard) ? this.getCardId(ruling.firstCard) : null;
    }

    getSecondCardId (ruling) {
        return (ruling.secondCard) ? this.getCardId(ruling.secondCard) : null;
    }

    getThirdCardId (ruling) {
        return (ruling.thirdCard) ? this.getCardId(ruling.thirdCard) : null;
    }

    getRulingCardsIds (ruling) {
        let ids = [];

        try {

            if (this.getFirstCardId(ruling)) {
                ids.push(this.getFirstCardId(ruling));
            }

            if (this.getSecondCardId(ruling)) {
                ids.push(this.getSecondCardId(ruling));
            }

            if (this.getThirdCardId(ruling)) {
                ids.push(this.getThirdCardId(ruling));
            }

        } catch (err) {
            Logger.warn('RulingListHelper.getRulingCardsIds problem', err);
        }

        return ids;
    }

    compareIds (cardId, index) {
        let { state } = this.props;
        let cards = state.cardList;


        // console.log(this.getCardId(ruling.firstCard), cards[index].id);
        // let ruling_card_id = this.getCardId(ruling.firstCard);
        // let

        return (typeof cards[index] !== 'undefined' && cards[index].id == cardId);
    }

    howManyTimes (card_ids, ruling_card_id) {
        try {
            return card_ids.filter(item => item == ruling_card_id).length;
        } catch (err) {
            Logger.warn('RulingListHelper.showRuling problem', err);
        }
    }

    showRuling (ruling, cards) {

        let card_ids = this.getRulingCardsIds(ruling);

        try {
            let ruling_cards = cards.filter((card) => {
                return card && card_ids.includes(card.id);
            });
            return (ruling_cards.length > 0)

        } catch (err) {
            throw new Error(err);
        }

    }

    getRulingHeader (ruling, cards, counter) {

        let card_ids = _.toArray( this.getRulingCardsIds(ruling) );
        let ruling_cards = _.toArray( _.unique(cards, 'id') );
        let text = '';
        console.log(ruling_cards);

        let card_names = ruling_cards.forEach((card, index) => {
            if (card) {

                if (index == 0) {
                    text = card.name;
                } else {
                    text = (this.howManyTimes(card_ids, card.id) === 1) ? ' & ' + card.name : '';
                }

            }
        });

        let grouped_rullings = _.groupBy(ruling_cards, 'name');

        //console.log(grouped_rullings);

        // let grouped_rullings = _.groupBy(ruling_cards, 'name');
        // let card_names_arr = _.keys( grouped_rullings );
        // let card_names = _.reduceRight(card_names_arr, (a, b) => {
        //     return a.concat(b);
        // }, []);
        // let ids = _.filter(card_names, (name) => {
        //     console.log(ruling.id, counter);
        //     return (ruling.id == name && counter == 0)
        // });
        //
        // text = (name.length) ? name[0] : '';
        // console.log(card_names, text, ids);

        return text;
    }
}

let helper = new RulingListHelper();

export default helper;