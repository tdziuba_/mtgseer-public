/**
 * Created by Tomasz Dziuba based on Paul Irish log function.
 *
 * http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
 * USAGE: Logger.log(variable)
 */

let instance: LoggerClass = null;

class LoggerClass {
    history: Array<any> = [];

    constructor() {
        if (!instance){
            instance = this;
        }

        return instance;
    }

    log () {
        this.history.push(arguments);

        if (window.console && typeof window.console.log === 'function') {
            console.log( Array.prototype.slice.call(arguments) );
        }
    }

    warn () {
        this.history.push(arguments);

        if (window.console && typeof window.console.warn === 'function') {
            throw new Error( Array.prototype.slice.call(arguments) )
        }
    }

    debug () {
        this.history.push(arguments);

        if (window.console && typeof window.console.debug === 'function') {
            console.debug( Array.prototype.slice.call(arguments) );
        }
    }
}

let Logger = new LoggerClass();
export default Logger;
