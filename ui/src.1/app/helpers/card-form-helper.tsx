/**
 * Created by Tomasz Dziuba on 2017-06-04.
 */
import _ from 'underscore';

export function handleLoadingState (isLoading, autocompleter) {
    if (isLoading) {
        autocompleter.setState({ isLoading: true });

        return;
    }

    autocompleter.setState({ isLoading: false });
}

export function compareOnDebounce (firstValue, secondValue, callback) {
    if (firstValue !== secondValue) {
        return;
    }

    callback();
}

// export function waitOnKeyUp (ev, counter, callback) {
//     console.log(ev.currentTarget.value, oldValue);
//     if (ev.currentTarget.value === oldValue) {
//         return oldValue;
//     }
//
//     return _.debounce(() => {
//         return (ev.currentTarget.value === oldValue) ? callback(ev) : waitOnKeyUp(ev, oldValue, callback);
//     }, 300);
// }
