
export function addEvent(element, eventType, callback) {
    try {
        if (element) {
            if (element.addEventListener) {
                element.addEventListener(eventType, callback, false)
            } else {
                element.attachEvent('on' + eventType, callback)
            }
        }
    } catch (err) {
        if (typeof console.warn !== 'undefined') console.warn(err, element);
    }
}

export function removeEvent(element, eventType, callback) {
    try {
        if (element) {
            if (element.removeEventListener) {
                element.removeEventListener(eventType, callback)
            } else {
                element.detachEvent('on' + eventType, callback)
            }
        }
    } catch (err) {
        if (typeof console.warn !== 'undefined') console.warn(err, element);
    }
}

export function stopEvent (event) {
    let ev = event || window.event;

    ev.cancelBubble = true;

    if (ev.stopPropagation) {
        ev.stopPropagation();
    }

}

export function preventEvent(event) {
    let ev = event || window.event;

    if (ev.preventDefault) {
        ev.preventDefault();
    }
}
