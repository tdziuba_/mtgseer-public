import * as React from 'react';
import { Route } from 'react-router';
// import * as views from './views/index';
// import NotFound from './components/NotFound';
import About from './components/About';
import { App } from './views/index';

export default (
    <Route path="/" component={ App }>
        {/* <Route exact path="/" component={ views.HowItWorks } />*/}
        {/*<Route path="/new-ruling" component={ views.SubmitRuling } />*/}
        <Route path="/about" component={ About } />
        {/*<Route path="/submit-question" component={ views.SubmitQuestion } />*/}
        {/*<Route path="/submit-ruling" component={ views.SubmitRuling } /> */}
        {/*<Route path="*" component={ NotFound } />*/}
    </Route>
);