/**
 * Created by Tomasz Dziuba on 2016-12-23.
 */
import path from "path";
import fs from "fs";
import * as Express from "express";
import React from "react";
import * as storage from "redux-storage";
import createEngine from "redux-storage-engine-localstorage";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { renderToString } from "react-dom/server";
import { match, RouterProps, RouterContext, Router, Route, IndexRoute, browserHistory, matchPath } from "react-router";
import ReduxThunk from "redux-thunk";
import * as routes from "../src/app/routes";
import initialState from "../src/app/initial-state";
import * as rootReducer from "../src/app/reducers";
import * as cors from "cors";
import * as helmet from "helmet";
import * as compression from "compression";

let app = Express();
let config = require("../config.json")[process.env["NODE_ENV"] || "development"];
const port: number = 3000;
const host: string = config.hasOwnProperty("UI_URI") ? config.UI_URI : "http://localhost:3000";

app.use(compression());
app.use(helmet());
app.use(
    cors({
        origin: "http://localhost:3000",
        optionsSuccessStatus: 200,
    })
);
app.use("/assets", Express.static(path.resolve(__dirname + "/../dist")));

// This is fired every time the server side receives a request
app.get("*", (req, res, next) => {
    if (path.extname(req.path).length > 0) {
        next();
    } else {
        handleRender(req, res, next);
    }
});

app.on("error", onError);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error): void {
    if (error.syscall !== "listen") {
        throw error;
    }

    let bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}

// We are going to fill these out in the sections to follow
function handleRender(reqt, res, next) {
    matchPath({ routes: routes, location: req.url }, (err, redirect, props) => {
        // Create a new Redux store instance

        // const middleware = [ ReduxThunk ];
        // const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

        const reducer = storage.reducer(rootReducer);
        const engine = createEngine("mtgseer");
        const storageMiddleware = storage.createMiddleware(engine);

        const middleware = [ReduxThunk, storageMiddleware];

        const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

        // Render the component to a string
        const html: string = renderToString(
            <Provider store={store}>
                <RouterContext {...props} />
            </Provider>
        );

        // Grab the initial state from our Redux store
        const preloadedState = store.getState();
        //preloadedState.config = config;

        // Send the rendered page back to the client
        res.send(renderFullPage(html, preloadedState));
    });
}

function renderFullPage(html, preloadedState) {
    preloadedState.config = config;

    return `
    <!doctype html>
    <html>
      <head>
        <title>MTG Seer - comprehensive MTG library</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round&amp;subset=latin-ext" rel="stylesheet">
        <link href="/assets/css/index.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        
        <link rel="apple-touch-icon" sizes="57x57" href="/assets/img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/assets/img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/assets/img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/assets/img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/assets/img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/assets/img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/assets/img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicon-16x16.png">
        <link rel="manifest" href="/assets/img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/assets/img/ms-icon-144x144.png">
        <meta name="theme-color" content="#003c4e">

        <!--[if lt IE 9]>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->

      </head>
      <body class="bodyElement">
        <div id="root">${html}</div>
        <script>
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
        </script>	
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/index.js"></script>
      </body>
    </html>
    `;
}

app.listen(port);

console.log("Server is running on " + host + " Use CTRL + C to close it.");
