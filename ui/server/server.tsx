import * as path from 'path';
import * as Express from 'express';
import * as webpack from 'webpack';
import middleware from './middleware';

import Chalk from 'Chalk';

const config = require("../config.json")[process.env["NODE_ENV"] || "development"];
const port: number = config.hasOwnProperty("UI_PORT") ? config.UI_PORT : 3000;
const host: string = config.hasOwnProperty("UI_HOST") ? config.UI_HOST : "http://localhost";

const app: Express.Express = Express();

if(process.env.NODE_ENV === 'production') {
    app.use(Express.static(path.resolve(__dirname, 'dist')));
} else {
    const config = require('../webpack.config.js');
    const compiler = webpack(config);

    app.use(require('webpack-dev-middleware')(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath,
        stats: {
            assets: false,
            colors: true,
            version: false,
            hash: false,
            timings: false,
            chunks: false,
            chunkModules: false
        }
    }));
    app.use(require('webpack-hot-middleware')(compiler));
    app.use(Express.static(path.resolve(__dirname, 'src')));
}
// if(process.env.NODE_ENV === 'development') {
// 	const config = require('../webpack.config.js');
// 	const compiler = webpack(config);
// 	app.use(require('webpack-dev-middleware')(compiler, {
// 		noInfo: true,
// 		publicPath: config.output.publicPath,
// 		stats: {
// 			assets: false,
// 			colors: true,
// 			version: false,
// 			hash: false,
// 			timings: false,
// 			chunks: false,
// 			chunkModules: false
// 		}
// 	}));
// 	app.use(require('webpack-hot-middleware')(compiler));
// 	app.use(express.static(path.resolve(__dirname, 'src')));
// } else if(process.env.NODE_ENV === 'production') {
// 	app.use(express.static(path.resolve(__dirname, 'dist')));
// }

app.get('*', middleware);

app.listen(port, (err) => {
	if (err) {
        console.error(Chalk.bgRed(err.message));
      } else {
        console.info(Chalk.black.bgGreen(
          `\n\n💂  Server is running on ${host}:${port}\n Use CTRL + C to close it.`,
        ));
    }
});




// import * as path from "path";
// import * as fs from "fs";
// import * as Express from "express";
// import * as React from "react";
// import * as storage from "redux-storage";
// import createEngine from "redux-storage-engine-localstorage";
// import { createStore, applyMiddleware } from "redux";
// import { Provider } from "react-redux";
// import { renderToString, renderToStaticMarkup } from "react-dom/server";
// import { Router, withRouter, match } from 'react-router';
// import { syncHistoryWithStore } from 'react-router-redux';
// const { ReduxAsyncConnect, loadOnServer } = require('redux-connect');
// import ReduxThunk from "redux-thunk";
// import * as routes from "../src/app/routes";
// import initialState from "../src/app/initial-state";
// import rootReducer from "../src/app/reducers";
// import * as cors from "cors";
// import * as helmet from "helmet";
// import * as compression from "compression";
// import * as Chalk from 'Chalk';

// let app = Express();
// let config = require("../config.json")[process.env["NODE_ENV"] || "development"];
// const port: number = 3000;
// const host: string = config.hasOwnProperty("UI_URI") ? config.UI_URI : "http://localhost:3000";

// app.use(compression());
// app.use(helmet());
// app.use(
//     cors({
//         origin: `${host}:${port}`,
//         optionsSuccessStatus: 200,
//     })
// );
// app.use("/assets", Express.static(path.resolve(__dirname + "/../dist")));

/*
app.get("*", (req, res, next) => {
    if (path.extname(req.path).length > 0) {
        next();
    } else {
        handleRender(req, res, next);
    }
});

app.on("error", onError);

/**
 * Event listener for HTTP server "error" event.
 */
/*
function onError(error): void {
    if (error.syscall !== "listen") {
        throw error;
    }

    let bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}

// We are going to fill these out in the sections to follow
function handleRender(req, res, next) {
    const location = req.url;
    const reducer = storage.reducer(rootReducer);
    const engine = createEngine("mtgseer");
    const storageMiddleware = storage.createMiddleware(engine);

    const middleware = [ReduxThunk, storageMiddleware];

    const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

    //const memoryHistory = createMemoryHistory(req.originalUrl);
    //const store = configureStore(memoryHistory);
    //const history = syncHistoryWithStore(memoryHistory, store);

    match({ history, routes, location },
        (error, redirectLocation, renderProps) => {
          if (error) {
            res.status(500).send(error.message);
          } else if (redirectLocation) {
            res.redirect(302, redirectLocation.pathname + redirectLocation.search);
          } else if (renderProps) {
            const asyncRenderData = Object.assign({}, renderProps, { store });
    
            loadOnServer(asyncRenderData).then(() => {
              const markup = renderToString(
                <Provider store={store} key="provider">
                  <ReduxAsyncConnect {...renderProps} />
                </Provider>,
              );
              res.status(200).send(renderHTML(markup, store));
            });
          } else {
            res.status(404).send('Not Found?');
          }
    });

    

    
    // matchPath({ routes: routes, location: req.url }, (err, redirect, props) => {
    //     // Create a new Redux store instance

    //     // const middleware = [ ReduxThunk ];
    //     // const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

    //     const reducer = storage.reducer(rootReducer);
    //     const engine = createEngine("mtgseer");
    //     const storageMiddleware = storage.createMiddleware(engine);

    //     const middleware = [ReduxThunk, storageMiddleware];

    //     const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

    //     // Render the component to a string
    //     const html: string = renderToString(
    //         <Provider store={store}>
    //             <RouterContext {...props} />
    //         </Provider>
    //     );

    //     // Grab the initial state from our Redux store
    //     const preloadedState = store.getState();
    //     //preloadedState.config = config;

    //     // Send the rendered page back to the client
    //     res.send(renderFullPage(html, preloadedState));
    // });
}

function renderHTML(markup, store) {
    // Load contents of index.html
    fs.readFile("./index.html", "utf8", function(err, data) {
        if (err) throw err;

        const document = data.replace();

        // Sends the response back to the client
        res.send(document);
    });
}

app.listen(port, host, (err) => {
    if (err) {
        console.error(Chalk.bgRed(err));
      } else {
        console.info(Chalk.black.bgGreen(
          `\n\n💂  Server is running on http://${host}:${port}\n Use CTRL + C to close it.`,
        ));
      }
});
*/
