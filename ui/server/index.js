/**
 * Created by Tomasz Dziuba on 2016-12-23.
 */
// require("babel-register");
// require("./server.ts");

require("@babel/register", {
    extensions: [".js", ".jsx", ".ts", ".tsx"],
});
require("./server.ts");
