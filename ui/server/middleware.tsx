import * as React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { store } from '../src/app/index';
import App from '../src/app/views/App';
import reducers from '../src/app/reducers';
import initialState from "../src/app/initial-state";

declare var window;

const preloadedState = store.getState();
function renderForDevelopment(req) {
    return (
        `
        <!doctype html>
        <html>
            <head>
                <title>My Universal App</title>
                <link rel='stylesheet' href='/css/index.min.css'>
            </head>
            <body>
                <div id='root'>${renderToString(
                    <Provider store={store}>
                        <StaticRouter location={req.url} context={{}}>
                            <App />
                        </StaticRouter>
                    </Provider>
                )}</div>
                <script>
                    window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
                </script>
            </body>
        </html>
		`
    )
}

function renderForProduction(req) {
    return (
        `
        <!doctype html>
			<html>
				<head>
					<title>My Universal App</title>
				</head>
				<body>
					<div id='root'></div>
					<script>
                        window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
                    </script>
                    <script src='/js/common.js'></script>
                    <script src='/js/index.js'></script>
				</body>
			</html>
		`
    )
}

export default (req, res) => {
	if (process.env.NODE_ENV === 'development') {
		res.send( renderForDevelopment(req) );
	} else {
		res.send( renderForProduction(req) );
	}
};