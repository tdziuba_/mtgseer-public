# Instalacja

1. Do działania potrzebny jest nodejs z menadżerem npm lub yarn (zalecany) oraz zainstalowanymi globalnie modułami:
babel, babel-cli, webpack

```npm install -g webpack babel babel-cli testcafe```

2. potem należy zainstalować resztę zależności z pliku package.json, czyli:

```npm install``` lub ```yarn install```

## Uruchomienie

```npm run start``` lub ```yarn run start```

```npm run build``` lub ```yarn run build```

wersja deweloperska powinna być uruchomiona z polecenia: 
```yarn run dev``` lub ```npm run dev```

