/**
 * Created by Tomasz Dziuba on 2016-12-23.
 */

var path = require("path"),
    webpack = require("webpack"),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    sassLoaderPaths = [path.resolve(__dirname, "./scss")],
    ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

sassLoaderPaths = sassLoaderPaths.concat(path.resolve(__dirname, "/node_modules/font-awesome/scss"));

module.exports = {
    devtool: "cheap-module-source-map",
    debug: false,
    entry: {
        index: ["./src/app/index.jsx"],
        common: ["react", "react-dom", "react-router", "react-redux", "axios"],
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "js/[name].js",
        chunkFilename: "js/[name].chunk.js",
        publicPath: "/",
    },
    sassLoader: {
        includePaths: sassLoaderPaths,
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
            { test: /\.jsx?$/, loader: "babel-loader", exclude: /node_modules/ },
            { test: /\.tsx?$/, loader: "ts-loader", exclude: /node_modules/, options: { transpileOnly: true } },
            { test: /\.scss$/, loader: ExtractTextPlugin.extract("style-loader", "css?sourceMap!sass-loader?sourceMap") },
            {
                test: /\.json$/,
                loaders: ["json"],
                exclude: /node_modules/,
                include: __dirname,
            },

            { test: /\.rt$/, loader: "react-templates-loader?targetVersion=0.15.1&modules=es6" },

            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=image/svg+xml&name=assets/fonts/[name].[ext]" },
            { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff&name=assets/fonts/[name].[ext]" },
            { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff&name=assets/fonts/[name].[ext]" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/octet-stream&name=assets/fonts/[name].[ext]" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?name=assets/fonts/[name].[ext]" },

            //img
            { test: /\.(png|gif|jpg)$/, loader: "file-loader?name=img/[name].[ext]" },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production"),
            },
        }),
        new ForkTsCheckerWebpackPlugin({
            workers: ForkTsCheckerWebpackPlugin.TWO_CPUS_FREE,
            watch: ["./src", "./test"], // optional but improves performance (less stat calls)
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin(),

        new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 15 }),
        new webpack.optimize.MinChunkSizePlugin({ minChunkSize: 10000 }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            children: true,
            async: true,
        }),

        new ExtractTextPlugin("css/[name].css", {
            allChunks: true,
            disable: false,
        }),
    ],
    resolveLoader: {
        alias: {
            "react-proxy": path.join(__dirname, "./src"),
        },
    },
    resolve: {
        extensions: ["", ".webpack.js", ".js", ".jsx"],
    },
};
