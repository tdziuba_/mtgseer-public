/**
 * Created by Tomasz Dziuba on 2016-12-23.
 */

var path = require("path"),
    webpack = require("webpack"),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin"),
    CopyWebpackPlugin = require("copy-webpack-plugin"),
    CleanWebpackPlugin = require("clean-webpack-plugin"),
    sassLoaderPaths = [path.resolve(__dirname, "./scss")];

sassLoaderPaths = sassLoaderPaths.concat(path.resolve(__dirname, "/node_modules/font-awesome/scss"));

module.exports = {
    entry: {
        index: ["./src/app/index.tsx"],
        common: ["es6-promise/auto", "react", "react-dom", "react-router", "react-redux", "axios"],
    },

    devtool: "cheap-module-source-map",

    output: {
        path: path.join(__dirname, "dist"),
        filename: "js/[name].js",
        chunkFilename: "js/[name].chunk.js",
        publicPath: "http://localhost:80/",
    },

    module: {
        rules: [
            // { test: /\.jsx?$/, loader: "babel-loader", exclude: /node_modules/ },
            // { test: /\.tsx?$/, loader: "ts-loader", exclude: /node_modules/, options: { transpileOnly: true } },
            { test: /\.(t|j)sx?$/, use: { loader: "ts-loader", options: { transpileOnly: true } }, exclude: /node_modules/ },
            // addition - add source-map support
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            {
                test: /\.s?css$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true,
                                modules: false,
                                importLoaders: 1,
                            },
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                includePaths: sassLoaderPaths,
                            },
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                parser: "postcss-scss",
                                syntax: require("postcss-scss"),
                                processors: function() {
                                    require("postcss-strip-inline-comments");
                                },
                                plugins: function() {
                                    return [require("autoprefixer")];
                                },
                            },
                        },
                    ],
                }),
            },
            {
                test: /\.json$/,
                loaders: ["json"],
                exclude: /node_modules/,
                include: __dirname,
            },

            { test: /\.rt$/, loader: "react-templates-loader?targetVersion=0.15.1&modules=es6" },

            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=image/svg+xml&name=assets/fonts/[name].[ext]" },
            { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff&name=assets/fonts/[name].[ext]" },
            { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff&name=assets/fonts/[name].[ext]" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/octet-stream&name=assets/fonts/[name].[ext]" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?name=assets/fonts/[name].[ext]" },

            //img
            { test: /\.(png|gif|jpg)$/, loader: "file-loader?name=img/[name].[ext]" },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(["./dist"]),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("development"),
                CONF: require("./config.json")["development"],
            },
        }),

        new ForkTsCheckerWebpackPlugin({
            workers: ForkTsCheckerWebpackPlugin.TWO_CPUS_FREE,
            watch: ["./src", "./test"], // optional but improves performance (less stat calls)
        }),

        new webpack.LoaderOptionsPlugin({
            // test: /\.xxx$/, // may apply this only for some modules
            options: {
                devtool: "eval-source-map",
                debug: true,
                watch: true,
                aggregateTimeout: 300,
                poll: 50000,
                sassLoader: {
                    includePaths: sassLoaderPaths,
                },
            },
        }),

        new ExtractTextPlugin({
            filename: "css/[name].css",
            disable: false,
            allChunks: true,
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: "common",
            filename: "js/common.js",
            minChunks: 2,
        }),

        new CopyWebpackPlugin(
            [
                { from: "./src/img", to: "./dist/img" },
                { from: "./src/manifest.json", to: "./dist/manifest.json" },
                { from: "./src/browserconfig.xml", to: "./dist/browserconfig.xml" },
            ],
            {}
        ),
    ],

    resolve: {
        extensions: [".webpack.js", ".jsx", ".ts", ".tsx", ".scss", ".js"],
        alias: {
            "react-proxy": path.join(__dirname, "./src"),
        },
    },
};
