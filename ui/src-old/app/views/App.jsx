import * as React from 'react';
import { Component } from 'react';
import { connect, ProviderProps } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/ruling-actions';
import Header from '../components/Header';

class App extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount () {

    }

    render () {
        let { children, state } = this.props;

        return (
            <div className="SL-app">
                <Header state= {state } parent={ this } />
                <section className="SL-content">
                    { children }
                </section>
                <footer className="SL-footer"><p>Copyrights &copy; { this.getYear() } by MTG Seer </p></footer>
            </div>
        );
    }

    getYear() {
        let date = new Date();

        return date.getFullYear();
    }

}

const mapStateToProps = (state) => ({
    state: state
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
