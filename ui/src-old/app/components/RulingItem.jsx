/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-04-02
 * Time: 11:05
 */

import * as React from 'react';
import { Component } from 'react';

export default function RulingItem () {

    this.renderHeader = (headerText) => {
        if (!headerText || headerText === '') {
            return;
        }

        return (
            <h3>{ headerText }</h3>
        );

    }

    let { ruling, headerText } = this.props;

    return (

        <div className="rulingItem">
            { this.renderHeader(headerText) }
            <p>{ ruling.text }</p>
            <p className="basedOn"><strong>Based on: { ruling.basedOn }</strong> by { ruling.author }</p>
        </div>
    );
}
// class RulingItem extends Component {
//     constructor(props) {
//         super(props);
//     }
//
//     render() {
//
//         let { ruling, headerText } = this.props;
//
//         return (
//             <div className="rulingItem">
//
//                 { this.renderHeader(headerText) }
//
//                 <p>{ ruling.text }</p>
//                 <p className="basedOn"><strong>Based on: { ruling.basedOn }</strong> by { ruling.author }</p>
//             </div>
//         );
//     }
//
//     renderHeader (headerText) {
//
//         if (headerText !== '') {
//            return (
//                <h3>{ headerText }</h3>
//            );
//         } else {
//             return null;
//         }
//
//     }
// }
//
// export default RulingItem;
