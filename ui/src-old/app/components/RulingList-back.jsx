/**
 * Created by Citiboard AB Team.
 * User: User
 * Date: 17.03.2017
 */

import React, { Component } from 'react';
import { Link } from 'react-router';
import Logger from '../helpers/Logger';
import RulingItem from './RulingItem';

class RulingList extends Component {
    constructor(props) {
        super(props)

        this.parent = this.props.parent;

        this.state = {
            headers: []
        };
    }

    getCardId (url) {
        return parseInt( url.replace('/cards/', '') );
    }

    getFirstCardId (ruling) {
        return (ruling.firstCard) ? this.getCardId(ruling.firstCard) : null;
    }

    getSecondCardId (ruling) {
        return (ruling.secondCard) ? this.getCardId(ruling.secondCard) : null;
    }

    getThirdCardId (ruling) {
        return (ruling.thirdCard) ? this.getCardId(ruling.thirdCard) : null;
    }

    getRulingCardsIds (ruling) {
        let ids = [];

        if (this.getFirstCardId(ruling)) {
            ids[0] = this.getFirstCardId(ruling);
        }

        if (this.getSecondCardId(ruling)) {
            ids[1] = this.getSecondCardId(ruling);
        }

        if (this.getThirdCardId(ruling)) {
            ids[2] = this.getThirdCardId(ruling);
        }

        return ids;
    }

    compareIds (cardId, index) {
        let { state } = this.props;
        let cards = state.cardList;


        // console.log(this.getCardId(ruling.firstCard), cards[index].id);
        // let ruling_card_id = this.getCardId(ruling.firstCard);
        // let

        return (typeof cards[index] !== 'undefined' && cards[index].id == cardId);
    }

    setRulingHeaders (headers) {
        this.setState({
            headers: headers
        });
    }

    showRuling (ruling) {
        let card_ids = this.getRulingCardsIds(ruling);
        let cards = this.props.state.cardList.filter((card) => {
            return card !== null;
        });
        let show_ruling = false;

        if (cards.length && card_ids.length) {
            if (cards.length > 1) {
                let tmp = [];

                card_ids.forEach((id, i) => {
                    if (card.id == id) {
                        tmp[i] = id;
                    }
                });
console.log(tmp, card_ids);
                show_ruling = (tmp.length == cards.length);

            } else {
                show_ruling = (cards[0].id == card_ids[0]);
            }

            // cards.forEach((card, index) => {
            //
            //     if (card) {
            //
            //         card_ids.forEach((id, i) => {
            //             if (card.id == id) {
            //                 show_ruling = true;
            //             }
            //         })
            //
            //     }
            //
            // });
        }
debugger;
        return show_ruling;
    }

    render() {
        const { state } = this.props;

        let cards = state.cardList;
        let self = this;
        let counter = 0;

        if (state.rulingList.length) {

            return (
                <article className="article rulinglist">
                    <div className="container">
                        <h2 className="articleTitle">Card rulings</h2>

                        {
                            state.rulingList.map((ruling) => {
                                counter++;

                                if ( self.showRuling(ruling) ) {
                                    return (
                                        <RulingItem key={ counter }
                                                        parent={ this }
                                                        ruling={ ruling }
                                                        headerText={self.renderRulingHeader(cards, ruling)} />
                                    );
                                }

                            })
                        }

                    </div>
                </article>
            );

        } else {

            return (
                <article className="article emptyRulinglist">
                    <div className="container">
                        <h2 className="articleTitle">Card rulings</h2>
                        <h3 className="brandName">No results!</h3>
                        <p><strong>Unfortunately there are no ruling for this set of cards. You could <Link to="new-ruling">add own ruling</Link> or <Link to="submit-question">submit a question</Link>.</strong></p>
                    </div>
                </article>
            );

        }
    }

    renderRulingHeader (cards, ruling) {
        let self = this,
            { state } = this.props,
            text = '',
            howManyTimesOccures = 0,
            headers = [],
            rulingIds = this.getRulingCardsIds(ruling);

        cards.forEach((card, index) => {
            if (card) {

                rulingIds.forEach((id, i) => {

                    if (id == card.id) {
                        howManyTimesOccures++;
                        text += (howManyTimesOccures == 1) ? card.name : ' & ' + card.name;
                        headers[index] = text;
                    }

                });

            }
        });

        // try {
        //
        //     if (cards.length) {
        //
        //         try {
        //             if (cards[0] && rulingIds.includes(cards[0].id) && !self.state.headers.includes(cards[0].name)) {
        //                 text = cards[0].name;
        //
        //                 let headers = self.state.headers;
        //                 headers.push(cards[0].name);
        //
        //                 self.parent.updateRulingsHeaders(headers);
        //
        //             } else {
        //                 text = '';
        //             }
        //         } catch (err) {
        //             Logger.warn(err);
        //         }
        //
        //         try {
        //             if (cards.length == 2) {
        //                 if ( cards[1] && rulingIds.includes(cards[1].id) && cards[0] && rulingIds.includes(cards[0].id) ) {
        //                     text = cards[0].name + ' & ' + cards[1].name;
        //
        //                     if () {
        //                         let headers = self.state.headers;
        //                         headers.push(cards[0].name);
        //
        //                         self.parent.updateRulingsHeaders(headers);
        //                     }
        //                 }
        //
        //                 if ( cards[1] && rulingIds.includes(cards[1].id) && (!cards[0] || !rulingIds.includes(cards[0].id)) ) {
        //                     text = cards[1].name;
        //                 }
        //             }
        //         } catch (err) {
        //             Logger.warn(err);
        //         }
        //
        //         try {
        //             if (cards.length == 3) {
        //                 if (rulingIds.includes(cards[2].id) && rulingIds.includes(cards[1].id) && rulingIds.includes(cards[0].id)) {
        //                     text = cards[0].name + ' & ' + cards[1].name + ' & ' + cards[2].name;
        //                 }
        //
        //                 if (rulingIds.includes(cards[2].id) && rulingIds.includes(cards[1].id) && !rulingIds.includes(cards[0].id)) {
        //                     text = cards[1].name + ' & ' + cards[2].name;
        //                 }
        //
        //                 if (rulingIds.includes(cards[2].id) && !rulingIds.includes(cards[1].id) && rulingIds.includes(cards[0].id)) {
        //                     text = cards[0].name + ' & ' + cards[2].name;
        //                 }
        //
        //                 if (rulingIds.includes(cards[2].id) && !rulingIds.includes(cards[1].id) && !rulingIds.includes(cards[0].id)) {
        //                     text = cards[2].name;
        //                 }
        //             }
                {/*} catch (err) {*/}
                    {/*Logger.warn(err);*/}
                {/*}*/}

        //     }
        //
        //     return (<h3>{ text }</h3>);
        //
        // } catch (err) {
        //     Logger.warn('renderRulingHeader problem', err);
        // }
        console.log(howManyTimesOccures);
        self.parent.updateRulingsHeaders(headers);

        return text;
    }

}

export default RulingList
