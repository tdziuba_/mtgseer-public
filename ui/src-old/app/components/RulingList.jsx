/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-04-04
 * Time: 20:05
 */

import * as React from 'react';
import { Component } from 'react';
import { Link } from 'react-router';
import Logger from '../helpers/Logger';
import RulingItem from './RulingItem';
import RulingListHelper from '../helpers/RulingListHelper';
import _ from 'underscore';

class RulingList extends Component {
    constructor(props) {
        super(props);

        this.parent = props.parent;

        this.state = {
            rulings: []
        };
    }

    render() {
        let { state } = this.props;
        let self = this;

        if (state.rulingList.length) {

            return (
                <article className="article rulinglist">
                    <div className="container">
                        <h2 className="articleTitle">Card rulings</h2>

                        { self.renderRulingList() }

                    </div>
                </article>
            );

        } else {

            return (
                <article className="article emptyRulinglist">
                    <div className="container">
                        <h2 className="articleTitle">Card rulings</h2>
                        <h3 className="brandName">No results!</h3>
                        <p><strong>Unfortunately there are no ruling for this set of cards. You could <Link to="new-ruling">add own ruling</Link> or <Link to="submit-question">submit a question</Link>.</strong></p>
                    </div>
                </article>
            );

        }

    }

    renderRulingList () {
        let { state } = this.props;
        let self = this;
        let filteredRulings;
        let headers = new Array(3);
        let header = '';
        let text = '';

        if (state.rulingList.length) {

            try {

                filteredRulings = _.uniq(state.rulingList, 'id');

                return filteredRulings.map((ruling, index) => {

                    if (RulingListHelper.showRuling(ruling, state.cardList)) {

                        header = RulingListHelper.getRulingHeader(ruling, state.cardList, index);
                        //console.log(header, headers, text);
                        // if (_.isArray(header)) {
                        //     _.each(header, (name) => {
                        //         if (!headers.includes(name)) {
                        //             text += name;
                        //
                        //             headers.push(name);
                        //         }
                        //     });
                        // }
                        //
                        // console.log(header, text);

                        // if ( !headers.includes(header[0]) ) {
                        //     text = header;
                        //     headers.push(header[0])
                        // } else {
                        //     text = '';
                        // }

                        return (
                            <RulingItem key={ index }
                                        parent={ self }
                                        ruling={ ruling }
                                        headerText={ text } />
                        );
                    }

                });

            } catch (err) {
                Logger.debug('RulingList renderRulingList problem', err);
            }
        }

    }


}

export default RulingList;
