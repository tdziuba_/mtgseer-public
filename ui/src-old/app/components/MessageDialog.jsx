/**
 * Created by Tomasz Dziuba.
 * Date: 22.03.2017
 */

import React, {Component} from 'react';
import Logger from '../helpers/Logger';

class MessageDialog extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {children} = this.props

        return (
            <div>
                { children }
            </div>
        )
    }
}

export default MessageDialog
