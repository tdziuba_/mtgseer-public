/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-11
 * Time: 11:05
 */

import * as React from 'react';
import { Component } from 'react';
import CardForm from './CardForm';
import { receiveCard, removeCard } from '../actions/card-actions';
import { receiveRuling, removeRuling } from '../actions/ruling-actions';
import axios from 'axios';
import { Logger } from '../helpers';

class Card extends Component {

    constructor (props) {
        super(props);

        this.parent = this.props.parent;

        this.state = {
            card: null,
            cid: null,
            showPicture: false
        };
    }

    componentDidMount () {
        let { state, cid } = this.props;

        this.setState({
            card: (typeof state.cardList[cid] !== 'undefined') ? state.cardList[cid] : null,
            cid: (typeof cid !== 'undefined') ? cid : null
        });
    }

    componentDidUpdate () {
        // console.log(this.state, this.props.state);
    }

    getCardState () {
        return this.props.state;
    }

    renderEmptySlot () {
        let { cid } = this.props;

        return (
            <div className={ this.getCardCssClass() }>
                <div className="main">
                    <div className="formWrp" onClick={ openCardForm }>
                        <div className="actionWrp">
                            <button className="btn btn-primary addCardBtn" title="Add card ruling"><i className="fa fa-plus" aria-hidden="true" /></button>
                            <p className="addNewCardText"><strong>Add</strong> {cid > 0 ? 'another': ''} card to ruling</p>
                        </div>
                    </div>
                </div>
                <CardForm ref="cardForm" parent={this} />
            </div>
        );
    }

    renderCard (card, cid) {

        return (
            <div className={ this.getCardCssClass() }>
                <div className="main">
                    <picture className="cardPicture">
                        <img className="cardImage" src={card.image} alt={card.name} />
                    </picture>
                    <h3 className="cardTitle">{card.name}</h3>
                    <p className="cardType"><strong>{card.type}</strong></p>
                    <div className="cardDescription">{card.text}</div>
                    <button ref="viewCard"
                            className="btn btn-default desktop-hide large-hide viewCardBtn"
                            title="Show card details"
                            onClick={ this.togglePicture.bind(this) }
                    >
                        <i className="fa fa-eye" aria-hidden="true" />
                    </button>
                    <button ref="removeCard"
                            className="btn btn-primary removeCardBtn"
                            title="Remove card from slot"
                            onClick={ this.removeCard.bind(this) }
                    >
                        <i className="fa fa-trash" aria-hidden="true" />
                    </button>
                </div>
            </div>
        );
    }

    render () {
        let { state, cid } = this.props,
            card = (state.cardList && typeof state.cardList[cid]) ? state.cardList[cid] : this.state.card;

        return (card) ? this.renderCard(card, cid) : this.renderEmptySlot();
    }

    openCardForm () {

        try {

            if (this.refs.hasOwnProperty('cardForm')) {
                this.refs.cardForm.setPopupVisibility(true).forceUpdate();
            }

        } catch (err) {
            Logger.warn('opening card form popup problem', err);
        }

    }

    closeCardForm () {

        try {

            if (this.refs.hasOwnProperty('cardForm')) {
                this.refs.cardForm.setPopupVisibility(false).forceUpdate();
            }

        } catch (err) {
            Logger.warn('closing card form popup problem', err);
        }

    }

    getCardCssClass () {
        let cssClass = 'SL-card-slot empty short';
        let { state, cid } = this.props;

        if ( this.state.card || (typeof state.cardList[cid] !== 'undefined' && state.cardList[cid] !== null) ) {
            cssClass = cssClass.replace(' empty', ' filled').trim();
        } else {
            cssClass = cssClass.replace(' filled', ' empty').trim();
        }

        if (!this.state.showPicture) {
            cssClass = cssClass.replace(' full', ' short').trim();
        } else {
            cssClass = cssClass.replace(' short', ' full').trim();
        }

        return cssClass;

    }

    getCardName (text) {
        let self = this;

        try {
            axios.get(window.__PRELOADED_STATE__.config.API_URI + '/cards?name=' + text)
                .then(function (response) {

                    if (response.status === 200 && response.statusText === 'OK') {
                        self.refs.cardForm.setAutocompleterSuggestions(response.data['hydra:member']);
                    }

                })
                .catch(function (error) {
                    Logger.warn('Something wrong with getting card name', error);
                });
        } catch (err) {
            Logger.warn('Something wrong with getting card name', err);
        }
    }

    togglePicture () {
        let showPic = !(this.state.showPicture);

        this.setState({ showPicture: showPic });
    }

    submitCard (data) {
        let self = this;
        let { dispatch, parent, state, cid } = this.props;

        axios.get(window.__PRELOADED_STATE__.config.API_URI + '/cards/' + data)
            .then(function (response) {
                self.refs.cardForm.refs.submitBtn.setButtonInReadyState();

                if (response.statusText === 'OK') {

                    try {
                        self.setState({card: response.data, cid: cid});
                        dispatch( receiveCard(response.data, cid) );
                        self.parent.setCard(response.data, cid);
                    } catch (err) {
                        Logger.warn('set card state problem', err);
                    }


                } else {
                    Logger.warn('Something wrong with getting card data', response);
                }

                self.closeCardForm();
            })
            .catch(function (error) {
                if (window.hasOwnProperty(console) && typeof window.console.warn !== 'undefined') window.console.warn(error);
            });


    }

    removeCard () {
        let { dispatch, state, cid } = this.props;

        try {
            dispatch( removeCard( this.state.card, cid ) );

            let rulingIds = state.cardList[cid].firstRulings;

            rulingIds.map(id => {
                dispatch( removeRuling( id.replace('/rulings/', '') ) );
            });

            this.setState({ card: null, cid: null });
        } catch (err) {
            Logger.warn('removing card problem', err);
        }

    }

}

export default Card;
