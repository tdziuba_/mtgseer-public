/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-20
 * Time: 07:32
 */

import * as React from 'react';
import { Component } from 'react';
import { addEvent, removeEvent, stopEvent, preventEvent } from '../helpers/CustomHandler';

class Button extends Component {
    isLoading = false;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isDisabled: false
        };
    }

    componentDidMount() {
        addEvent(this.refs.button, 'click', this.onClick.bind(this));
    }

    componentWillUnmount() {
        removeEvent(this.refs.button, 'click', this.onClick);
    }

    onClick(ev) {
        preventEvent(ev);
        removeEvent(this.refs.button, 'click', this.onClick);

        this.props.parent.onButtonClick(this);
    }

    setButtonInLoadingState() {
        this.setState({isLoading: true, isDisabled: true});

        return this;
    }

    setButtonInReadyState() {
        this.setState({isLoading: false, isDisabled: true});
        this.disableButton();
        this.refs.button.setAttribute('disabled', 'disabled');

        return this;
    }

    disableButton() {
        this.setState({isDisabled: true});

        return this;
    }

    enableButton() {
        this.setState({isDisabled: false});

        return this;
    }

    getCssClass() {
        let { cssClass } = this.props;

        return (this.state.isLoading) ? cssClass + ' loading' : cssClass;
    }

    render() {
        let { children } = this.props;

        if (this.state.isDisabled) {

            return (
                <button ref="button" className={ this.getCssClass() }  disabled="disabled">
                    <i className={(this.state.isLoading) ? 'fa fa-spin fa-circle-o-notch' : '' }/>
                    <strong className="btnText">{ children }</strong>
                </button>
            );

        } else {

            return (
                <button ref="button" className={ this.getCssClass() }>
                    <i className={(this.isLoading) ? 'fa fa-spin fa-circle-o-notch' : '' }/>
                    <strong className="btnText">{ children }</strong>
                </button>
            );

        }
    }
}

export default Button;
