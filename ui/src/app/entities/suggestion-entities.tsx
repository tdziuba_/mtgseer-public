import { CardEntity } from '.';

interface SuggestionEntity {
    id: number,
    name: string,
}

interface SuggestionListEntity {
    suggestions: Array<SuggestionEntity>,
    selected: CardEntity | null
}

interface SuggestionActionEntity {
    type: string;
    suggestions: Array<SuggestionEntity>;
    selected?: SuggestionEntity | null
}

export {
    SuggestionListEntity,
    SuggestionEntity,
    SuggestionActionEntity
}