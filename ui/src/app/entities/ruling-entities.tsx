export interface RulingEntity {
    id: number;
    author: string;
    date: string,
    createdAt: string,
    source: string,
    basedOn: string,
    text: string,
    verifiedAt: string,
    firstCard: string,
    secondCard: string,
    thirdCard: string,
    rulingTraslations: Array<string>,
    verifiedBy: string
}

export interface RulingActionEntity {
    type: string, 
    rulings?: Array<RulingEntity>
    ruling?: RulingEntity,
    rulingId?: number,
}