export interface CardEntity {
    id: number;
    name: string;
    image: string;
    text: string;
    gathererLink: string;
    type: string;
    rulings: string [];
    firstQuestions: string [];
    secondQuestions: string [];
    thirdQuestions: string [];
    multiverseid: number;
}

export interface CardResponseEntity {
    id: number;
    name: string;
    image: string;
    text: string;
    gathererLink: string;
    type: string;
    firstRulings: string [];
    secondRulings: string [];
    thirdRulings: string [];
    firstQuestions: string [];
    secondQuestions: string [];
    thirdQuestions: string [];
    multiverseid: number;
}

export interface CardActionEntity {
    type: string;
    card: CardEntity,
    cid: number;
}