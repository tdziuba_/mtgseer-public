import { CardEntity, CardResponseEntity, CardActionEntity } from './card-entities'
import { RulingEntity, RulingActionEntity } from './ruling-entities'
import { SuggestionListEntity, SuggestionEntity, SuggestionActionEntity } from './suggestion-entities'

interface StateEntity {
    cardList: Array<CardEntity>,
    rulingList: Array<RulingEntity>,
    suggestionList: SuggestionListEntity
}

export {
    CardEntity,
    CardResponseEntity,
    CardActionEntity,
    RulingEntity,
    RulingActionEntity,
    SuggestionListEntity,
    SuggestionEntity,
    SuggestionActionEntity,
    StateEntity,
}