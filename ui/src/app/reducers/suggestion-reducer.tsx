/**
 * Created by Tomasz Dziuba on 2017-06-04.
 */
import * as types from '../actions/action-constants';
import initialState from '../initial-state';
import { CardEntity, CardResponseEntity, StateEntity, SuggestionActionEntity } from '../entities'

export default function suggestionReducer(state = initialState.suggestionList, action: SuggestionActionEntity) {
    switch (action.type) {
        case types.RECEIVE_SUGGESTIONS:

            return {
                selected: null,
                suggestions: action.suggestions.map((card: CardResponseEntity) => {
                    return {
                        id: card.id,
                        name: card.name,
                        image: card.image,
                        type: card.type,
                        text: card.text,
                        gathererLink: card.gathererLink,
                        rulings: [...card.firstRulings, ...card.secondRulings, ...card.thirdRulings]
                    };
                })
            }

        case types.CLEAR_SUGGESTIONS:

            return {
                suggestions: [],
                selected: null
            };

        case types.SET_SUGGESTION:

            return {
                suggestions: [],
                selected: action.selected
            };

        default:
            return state;
    }
};
