import * as types from '../actions/action-constants';
import initialState from '../initial-state';
import Logger from '../helpers/Logger';
// import { arrayIncludes, removeFromArray } from '../helpers/array-includes';
import * as _ from 'underscore';
import { RulingActionEntity } from '../entities'

export default function rulingList(state = initialState.rulingList, action: RulingActionEntity) {
    switch(action.type) {
        case types.RECEIVE_SINGLE_RULING:

            try {
                return Array.from([
                    ...state,
                    action.ruling
                ]).filter((ruling, index, self) => self.findIndex((t) => {
                    return t.id === ruling.id && t.id === ruling.id;
                }) === index);
            } catch (err) {
                Logger.warn('receive single ruling reducer problem', err);
            }

        case types.REMOVE_RULING:

            try {
                let ruling = state.filter(data => {
                    return data.id === action.rulingId
                });

                return state.filter(item => {
                    return item !== ruling[0];
                });

            } catch (err) {
                Logger.warn('remove ruling reducer problem', err);
            }

        default:
            return state;
    }
}