import { combineReducers } from 'redux';

import rulingList from './ruling-reducer';
import cardList from './card-reducer';
import suggestionList from './suggestion-reducer';

//let config = (typeof window !== 'undefined') ? window.__PRELOADED_STATE__.config : null;


const rootReducer = combineReducers({
    //config,
    rulingList,
    cardList,
    suggestionList
});

export default rootReducer;