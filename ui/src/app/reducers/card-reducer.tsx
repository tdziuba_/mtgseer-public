import * as types from '../actions/action-constants';
import initialState from '../initial-state';
import { CardEntity, CardActionEntity } from '../entities/index';
import { Action } from 'redux';

export default function cardReducer(state = initialState.cardList, action: CardActionEntity) {
    switch(action.type) {
        case types.RECEIVE_SINGLE_CARD:

            return [
                ...state.slice(0, action.cid),
                action.card,
                ...state.slice(action.cid + 1)
            ];

        case types.REMOVE_CARD:
            let arr: Array<CardEntity> = [];

            state.forEach((card, index) => {
                if (index !== action.cid) {
                    arr[index] = card;
                }
            });

            return arr;

        default:
            return state;
    }
}