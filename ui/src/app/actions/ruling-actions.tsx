import * as types from './action-constants';
import { RulingEntity, RulingActionEntity } from '../entities'

export function receiveRulingList (rulings: Array<RulingEntity>): RulingActionEntity {
    return { type: types.RECEIVE_RULING_LIST, rulings }
}

export function receiveRuling (ruling: RulingEntity): RulingActionEntity {
    return { type: types.RECEIVE_SINGLE_RULING, ruling };
}

export function removeRuling (rulingId: number): RulingActionEntity {
    return { type: types.REMOVE_RULING, rulingId };
}