/**
 * Created by Tomasz Dziuba on 2017-06-04.
 */
import * as types from './action-constants';
import { SuggestionEntity } from '../entities/index';

export function receiveAutosugesstions (suggestions: Array<SuggestionEntity>) {
    return { type: types.RECEIVE_SUGGESTIONS, suggestions };
}

export function clearAutosugesstions () {
    const suggestions: Array<SuggestionEntity> = [];
    return { type: types.CLEAR_SUGGESTIONS, suggestions };
}

export function setSingleAutosugesstion (suggestion: SuggestionEntity) {
    return { type: types.SET_SUGGESTION, selected: suggestion };
}

export function clearSingleAutosugesstions () {
    const selected: null = null;
    return { type: types.CLEAR_SUGGESTION, selected };
}