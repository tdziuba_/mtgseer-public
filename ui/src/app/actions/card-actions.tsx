import * as types from './action-constants';
import { CardActionEntity, CardEntity} from '../entities/card-entities'

export function receiveCard (card: CardEntity, cid: number): CardActionEntity {
    return { type: types.RECEIVE_SINGLE_CARD, card, cid };
}

export function removeCard (card: CardEntity, cid: number): CardActionEntity {
    return { type: types.REMOVE_CARD, card, cid };
}
