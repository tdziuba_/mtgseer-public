/**
 * Created by Tomasz Dziuba on 2017-05-28.
 */
 
import * as types from './action-constants';
import { receiveCard, removeCard } from './card-actions';
import { receiveAutosugesstions, clearAutosugesstions } from './suggestion-actions';
import { receiveRuling, removeRuling } from './ruling-actions';
import { CardsApi } from '../api';
import Logger from '../helpers/Logger';
import { Dispatch } from 'redux';
import { StateEntity, CardEntity } from '../entities/index';
import axios from 'axios'

export function fetchAutosuggestions(text: string, callback: Function) {
    return (dispatch: Dispatch<StateEntity>, getState: Function) => {

        CardsApi.fetchAutosuggestions(text, callback, (err: Error) => Logger.warn('Something wrong with getting card name', err))
        // try {

        //     axios.get(`${ window.__PRELOADED_STATE__.config.API_URI }/cards?name=${ text }`)
        //         .then(response => {

        //             if (response.status === 200 && response.statusText === 'OK') {
        //                 dispatch( receiveAutosugesstions(response.data['hydra:member']) );

        //                 return callback();
        //             }

        //         })
        //         .catch(function (error) {
        //             Logger.warn('Something wrong with getting card name', error);
        //         });

        // } catch (err) {
        //     Logger.warn('Something wrong with getting card name', err);
        // }

    };
}

export function fetchRulingList (rulingAddresses: Array<string>, callback: Function) {
    return (dispatch: Dispatch<StateEntity>, getState: Function) => {
        rulingAddresses.forEach(address => {
            axios.get(window.__PRELOADED_STATE__.config.API_URI + address)
                .then(response => {
                    if (response.status === 200 && response.statusText === 'OK') {
                        dispatch(receiveRuling(response.data));

                        return (callback) ? callback() : null;
                    }
                })
                .catch(err => {
                    Logger.warn(err.message, err)
                });
        });
    };
}

export function getCardData (card: CardEntity, cid: number) {
    return (dispatch: Dispatch<StateEntity>, getState: Function) => {
        dispatch( receiveCard(card, cid) );
        dispatch( fetchRulingList(card.rulings, () => alert('received')) )
    };
}
