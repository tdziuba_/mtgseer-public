/**
 * Created by Tomasz Dziuba on 2017-06-04.
 */
import * as _ from 'underscore';
import { ReactInstance, ReactElement, Component } from 'react';

export function handleLoadingState (isLoading: boolean, autocompleter: Component) {
    if (isLoading) {
        autocompleter.setState({ isLoading: true });

        return;
    }

    autocompleter.setState({ isLoading: false });
}

export function compareOnDebounce (firstValue: any, secondValue: any, callback: Function) {
    if (firstValue !== secondValue) {
        return;
    }

    callback();
}

// export function waitOnKeyUp (ev, counter, callback) {
//     console.log(ev.currentTarget.value, oldValue);
//     if (ev.currentTarget.value === oldValue) {
//         return oldValue;
//     }
//
//     return _.debounce(() => {
//         return (ev.currentTarget.value === oldValue) ? callback(ev) : waitOnKeyUp(ev, oldValue, callback);
//     }, 300);
// }
