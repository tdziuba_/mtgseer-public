/**
 * Created by Tomasz Dziuba based on Paul Irish log function.
 *
 * http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
 * USAGE: Logger.log(variable)
 */
import { ErrorInfo} from 'react';

let instance: LoggerClass | null = null;

class LoggerClass {
    history: Array<any> = [];

    constructor() {
        if (!instance){
            instance = this;
        }

        return instance;
    }

    log () {
        this.history.push(arguments);

        if (window.console && typeof window.console.log === 'function') {
            console.log( Array.prototype.slice.call(arguments) );
        }
    }

    warn (info?: string | ErrorInfo, error?: Error) {
        this.history.push(arguments);

        if (window.console && typeof window.console.warn === 'function') {
            throw new Error( Array.prototype.slice.call(arguments) )
        }
    }

    debug (debuggable: any) {
        this.history.push(debuggable);

        if (window.console && typeof window.console.debug === 'function') {
            console.debug(debuggable);
        }
    }
}

let Logger = new LoggerClass();
export default Logger;
