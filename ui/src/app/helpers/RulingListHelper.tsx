
import * as _ from 'underscore';
import Logger from './Logger';
import { StateEntity, CardEntity, RulingEntity } from '../entities/index';

class RulingListHelper {

    constructor () {
        return {
            showRuling: this.showRuling,
            getRulingCardsIds: this.getRulingCardsIds,
            getFirstCardId: this.getFirstCardId,
            getSecondCardId: this.getSecondCardId,
            getThirdCardId: this.getThirdCardId,
            getCardId: this.getCardId,
            compareIds: this.compareIds,
            getRulingHeader: this.getRulingHeader,
            howManyTimes: this.howManyTimes
        }
    }

    getCardId (url: string) {
        return parseInt( url.replace('/cards/', '') );
    }

    getFirstCardId (ruling: RulingEntity) {
        return (ruling.firstCard) ? this.getCardId(ruling.firstCard) : null;
    }

    getSecondCardId (ruling: RulingEntity) {
        return (ruling.secondCard) ? this.getCardId(ruling.secondCard) : null;
    }

    getThirdCardId (ruling: RulingEntity) {
        return (ruling.thirdCard) ? this.getCardId(ruling.thirdCard) : null;
    }

    getRulingCardsIds (ruling: RulingEntity) {
        let ids: Array<number> = [];

        try {

            if (this.getFirstCardId(ruling)) {
                ids.push(this.getFirstCardId(ruling));
            }

            if (this.getSecondCardId(ruling)) {
                ids.push(this.getSecondCardId(ruling));
            }

            if (this.getThirdCardId(ruling)) {
                ids.push(this.getThirdCardId(ruling));
            }

        } catch (err) {
            Logger.warn('RulingListHelper.getRulingCardsIds problem', err);
        }

        return ids;
    }

    compareIds (cardId: number, index: number, state?: StateEntity): boolean {
        let cards = state.cardList;


        // console.log(this.getCardId(ruling.firstCard), cards[index].id);
        // let ruling_card_id = this.getCardId(ruling.firstCard);
        // let

        return (typeof cards[index] !== 'undefined' && cards[index].id == cardId);
    }

    howManyTimes (card_ids: Array<number>, ruling_card_id: number): number {
        try {
            return card_ids.filter(item => item == ruling_card_id).length;
        } catch (err) {
            Logger.warn('RulingListHelper.showRuling problem', err);
        }
    }

    showRuling (ruling: RulingEntity, cards: Array<CardEntity>) {

        let card_ids = this.getRulingCardsIds(ruling);

        try {
            let ruling_cards = cards.filter((card) => {
                return card && card_ids.includes(card.id);
            });
            return (ruling_cards.length > 0)

        } catch (err) {
            Logger.warn(err);
        }

    }

    getRulingHeader (ruling: RulingEntity, cards: Array<CardEntity>, counter: number) {

        let card_ids: Array<number> = _.toArray( this.getRulingCardsIds(ruling) );
        let ruling_cards: Array<CardEntity> = _.toArray( _.unique(cards, 'id') );
        let text = '';
        console.log(ruling_cards);

        ruling_cards.forEach((card: CardEntity, index: number) => {
            if (card) {

                if (index == 0) {
                    text = card.name;
                } else {
                    text = (this.howManyTimes(card_ids, card.id) === 1) ? ' & ' + card.name : '';
                }

            }
        });

        let grouped_rullings = _.groupBy(ruling_cards, 'name');

        //console.log(grouped_rullings);

        // let grouped_rullings = _.groupBy(ruling_cards, 'name');
        // let card_names_arr = _.keys( grouped_rullings );
        // let card_names = _.reduceRight(card_names_arr, (a, b) => {
        //     return a.concat(b);
        // }, []);
        // let ids = _.filter(card_names, (name) => {
        //     console.log(ruling.id, counter);
        //     return (ruling.id == name && counter == 0)
        // });
        //
        // text = (name.length) ? name[0] : '';
        // console.log(card_names, text, ids);

        return text;
    }
}

let helper = new RulingListHelper();

export default helper;