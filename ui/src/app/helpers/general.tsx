import { ArgumentNode } from "graphql";

/**
 * Created by Tomasz Dziuba on 2017-05-28.
 */

/**
 * returns scrollbar width
 * based on http://stackoverflow.com/questions/986937/how-can-i-get-the-browsers-scrollbar-sizes#answer-986977
 * @returns {number}
 */
export function getScrollbarWidth() {
    let el = document.createElement('div'),
        outer = document.createElement('div'),
        fullW, outerW;

    el.style.cssText = 'width: 100%; height: 1px;';
    outer.style.cssText = 'position: absolute; top:0; left:0; width: 100px; height: 1px; visibility: hidden; overflow: hidden;';
    outer.appendChild(el);
    document.body.appendChild(outer);

    fullW = outer.offsetWidth;

    outer.style.overflow = "scroll";

    outerW = outer.offsetWidth;

    if (fullW == outerW) outerW = outer.clientWidth;

    document.body.removeChild(outer);

    return Math.abs(fullW - outerW);
}
