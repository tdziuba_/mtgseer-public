/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-20
 * Time: 21:07
 */

import * as React from 'react';
import { Component, ReactNode, ReactPropTypes, ReactEventHandler, KeyboardEvent, ChangeEvent, ChangeEventHandler, InputHTMLAttributes } from 'react';
import { connect, Dispatch } from 'react-redux';
import { clearAutosugesstions, setSingleAutosugesstion } from '../actions/suggestion-actions';
import { fetchAutosuggestions } from '../actions/action-creators';
import { SuggestionListEntity, SuggestionEntity, StateEntity} from '../entities';
import { Logger } from '../helpers';
import { InputPropsInterface } from './CardForm';

interface ComponentState {
    isLoading: boolean;
    selected: null | SuggestionEntity;
    typedCardName: string;
}

interface AutocompleterProps extends ReactPropTypes {
    suggestionList: SuggestionListEntity;
    dispatch: Dispatch<any>;
    inputProps: InputPropsInterface;
}

class Autocompletter extends Component<AutocompleterProps, any> {

    input: HTMLInputElement;
    autocompletter: HTMLElement;
    state: ComponentState;
    t: number;
    onClose: Function;

    constructor (props: AutocompleterProps) {
        super(props);

        this.state = {
            isLoading: false,
            selected: null,
            typedCardName: '',
        }
    }

    componentDidUpdate (): void {
        const suggestionList: SuggestionListEntity = this.props.suggestionList;
        const autocompleter = (this.autocompletter as any);

        if (!suggestionList || !suggestionList.suggestions.length || !this.autocompletter) {
            return;
        }

        let top = this.autocompletter.scrollTop;

        document.body.scrollTop = top - 50;

    }

    componentWillUnmount (): void {
        this.clearSuggestions();
    }

    onChange (ev: ChangeEvent<any>): void {
        const dispatch: Dispatch<any> = this.props.dispatch;
        const target = ev.target as HTMLInputElement;
        const suggestion = JSON.parse(target.value);

        this.clearSuggestions();

        this.setState({
            selected: target.value,
            isLoading: false
        });

        this.input.value = suggestion.name;

        dispatch( setSingleAutosugesstion(suggestion) );

    }

    onKeyUp (ev: KeyboardEvent<any>): void {
        ev.persist();
        clearTimeout(this.t);

        const ESC_KEY: number = 27;
        const CARD_NAME: string = this.state.typedCardName;
        const TYPED_CARD_NAME: string = ev.currentTarget.value;
        const target = ev.target as HTMLInputElement;

        if (target.value.length < 3) {
            return;
        }

        if (ev.keyCode === ESC_KEY) {
            this.onClose();

            return;
        }

        this.t = setTimeout(() => {

            if (CARD_NAME ===  this.state.typedCardName) {
                return this.getAutocompletterSuggestions(ev);
            }

            this.setState({ typedCardName: TYPED_CARD_NAME });
        }, 500);

    }

    getAutocompletterSuggestions(ev: KeyboardEvent<any>): void {
        const dispatch: Dispatch<any> = this.props.dispatch,
              target = ev.target as HTMLInputElement,
              name: string = target.value;

        try {

            const callback = () => {
                this.setState({
                    isLoading: false
                })
            };

            this.setState({
                isLoading: true
            });

            dispatch( fetchAutosuggestions( name, callback ) );

        } catch (err) {
            Logger.warn('getautocompletterSuggestions problem', err);
        }

    }

    clearSuggestions (): any {
        const dispatch: Dispatch<any> = this.props.dispatch;

        this.setState({ selected: null, isLoading: false });

        dispatch(clearAutosugesstions());

    }

    renderList (): ReactNode {

        const suggestionList: SuggestionListEntity = this.props.suggestionList;

        if (!suggestionList.suggestions.length) {
            return;
        }

        let counter = 0,
            items = suggestionList.suggestions.map((item) => {
                counter++;
                return this.renderItem(counter, item);
            });

        return (
            <ul className="autocompleter-list">{ items }</ul>
        );
    }

    renderItem (index: number, item: SuggestionEntity): ReactNode {
        interface InputProps extends InputHTMLAttributes<any> {
            type: string;           
            className: string;
            name: string;
            autoFocus: boolean;
            value: string;
            onChange: ChangeEventHandler<any>;
        }

        const inputProps: InputProps = {
            type: "radio",             
            className: "radio-hidden",
            name: "name",
            autoFocus: true,
            value: JSON.stringify(item),
            onChange: (ev: ChangeEvent<any>) => { this.onChange(ev) }
        }

        return (
            <li key={index}
                className="autocompleter-item"
                data-id={ item.id }
            >
                <label className="suggestionLabel">
                    <input {...inputProps}/>
                    <span className="suggestion">{ item.name }</span>
                </label>
            </li>
        );
    }

    renderSpinner (): ReactNode | null {
        return (this.state.isLoading) ? <i className="fa fa-spinner fa-spin" aria-hidden="true" /> : null;
    }

    render (): ReactNode {
        const inputProps: InputPropsInterface =  {
            ...this.props.inputProps,
            //type: 'text',
            //ref: (input: HTMLInputElement) => { this.input = input },
            //onKeyUp: (ev: KeyboardEvent<any>) => this.onKeyUp(ev)
        }

        return (
            <div ref={ (autocompletter: HTMLDivElement) => { this.autocompletter = autocompletter }} className="autocompleter">

                { this.renderSpinner() }

                <input {...inputProps} />
                {/* <input type="text"
                       ref={ (input: HTMLInputElement) => { this.input = input } }
                       {...inputProps}
                       onKeyUp={ (ev: KeyboardEvent<any>) => this.onKeyUp(ev) }
                /> */}
                { this.renderList() }

            </div>
        );
    }
}

function mapStateToProps(state: StateEntity) {
    return { suggestionList: state.suggestionList }
}

export default connect(mapStateToProps, null, null, { withRef: true })(Autocompletter);

//
// export default connect((state) => ({
//     suggestionList: state.suggestionList
// }, null, null, {withRef: true}))(Autocompletter);
