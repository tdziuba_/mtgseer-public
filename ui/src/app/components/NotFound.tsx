import * as React from 'react';

const NotFound = function () {

    return (
        <div className="row">
            <article className="article">
                <div className="container">
                    <h2 className="articleTitle">Missing page</h2>
                    <p>Check address path or use navigation menu</p>
                </div>
            </article>
        </div>
    );

};

export default NotFound;
