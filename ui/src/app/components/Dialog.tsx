/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-03-24
 * Time: 08:41
 */

import React, { Component } from 'react';
import { addEvent, removeEvent, preventEvent } from '../helpers/CustomHandler';
import Button from './Button';
import Logger from '../helpers/Logger';

class Dialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
            message: '',
            title: ''
        };

        this.parent = this.props.parent;
    }

    componentDidMount () {
        addEvent(this.refs.popupLayer, 'click', this.onClose.bind(this));
        addEvent(this.refs.closeBtn,   'click', this.onClose.bind(this));
        addEvent(document,             'keyup', this.onKeyUp.bind(this));
    }

    componentWillUnmount() {
        removeEvent(this.refs.popupLayer, 'click', this.onClose);
        removeEvent(this.refs.closeBtn,   'click', this.onClose);
        removeEvent(document,             'keyup', this.onKeyUp);
    }

    onClose (ev) {
        preventEvent(ev);

        this.setState({
            showPopup: false
        });
    }

    /**
     * detecting esc key pressed
     * @param ev
     */
    onKeyUp (ev) {
        if (ev.keyCode == 27) {
            this.onClose(ev);
        }
    }

    show () {
        this.setState({
            showPopup: true
        });
    }

    close () {
        this.setState({
            showPopup: false
        });
    }

    setUpDialog (options) {
        this.setState(options);

        return this;
    }

    render () {

        if (this.state.showPopup) {
            return (
                <div className="popup dialogPopup">
                    <div className="popupBox">
                        <button ref="closeBtn" className="btn closeBtn"><i className="fa fa-times" aria-hidden="true"/>
                        </button>
                        <h3 className="popupHeader">{ this.state.title }</h3>
                        <p>{ this.state.message }</p>
                    </div>
                    <div ref="popupLayer" className="popupLayer"/>
                </div>
            );
        } else {
            return null;
        }

    }
}

export default Dialog;
