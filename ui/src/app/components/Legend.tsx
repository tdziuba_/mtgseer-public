/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-03-27
 * Time: 18:54
 */

import * as React from 'react';

const Legend = function ({children}) {
    return (
        <legend className="addRulingFormCaption"><strong>{ children }</strong></legend>
    );
}

export default Legend;
