/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-04-04
 * Time: 20:05
 */

import * as React from 'react';
import { Component, ReactPropTypes } from 'react';
import { Link } from 'react-router-dom';
import Logger from '../helpers/Logger';
import RulingItem from './RulingItem';
import RulingListHelper from '../helpers/RulingListHelper';
import * as _ from 'underscore';
import { CardEntity, RulingEntity } from '../entities';

export interface RulingListProps extends ReactPropTypes {
    rulingList: Array<RulingEntity>;
    cardList: Array<CardEntity>;
}
class RulingList extends Component<RulingListProps, any> {
    constructor(props: RulingListProps) {
        super(props);

        this.state = {
            rulings: []
        };
    }

    render(): JSX.Element {
        const rulingList = this.props.rulingList;

        return (!rulingList.length) ? this.renderEmptyList() : this.renderRulingListWrapper();
    }

    renderEmptyList (): JSX.Element {
        return (
            <article className="article emptyRulinglist">
                <div className="container">
                    <h2 className="articleTitle">Card rulings</h2>
                    <h3 className="brandName">No results!</h3>
                    <p><strong>Unfortunately there are no ruling for this set of cards. You could <Link to="new-ruling">add own ruling</Link> or <Link to="submit-question">submit a question</Link>.</strong></p>
                </div>
            </article>
        );
    }

    renderRulingListWrapper (): JSX.Element {
        return (
            <article className="article rulinglist">
                <div className="container">
                    <h2 className="articleTitle">Card rulings</h2>

                    { this.renderRulingList() }

                </div>
            </article>
        );
    }

    renderRulingList (): Array<JSX.Element> | null {
        const rulingList: Array<RulingEntity> = this.props.rulingList;
        const cardList: Array<CardEntity> = this.props.cardList;
        
        let filteredRulings: Array<RulingEntity> = [];
        let headers = new Array(3);
        let header: string = '';
        let text: string = '';

        if (!rulingList.length) {
            return null;
        }

        filteredRulings = _.uniq(rulingList, 'id');

        // let items = filteredRulings.filter((ruling, index) => {
        //     return RulingListHelper.showRuling(ruling, cardList)
        // });

        return filteredRulings
            .filter((ruling: RulingEntity) => {
                return RulingListHelper.showRuling(ruling, cardList)
            })
            .map((item: RulingEntity, index: number) => {

                header += RulingListHelper.getRulingHeader(item, cardList, index);

                return (
                  <RulingItem
                      key={index}
                      ruling={item}
                      headerText={header}
                  />
                );
            });

        // if (rulingList.length) {
        //
        //     try {
        //
        //         filteredRulings = _.uniq(rulingList, 'id');
        //
        //         let items = filteredRulings.filter((ruling, index) => {
        //             return RulingListHelper.showRuling(ruling, cardList)
        //         });
        //         console.log(items);
        //
        //         return null;
        //
        //         // return filteredRulings.map((ruling, index) => {
        //         //
        //         //     if (RulingListHelper.showRuling(ruling, cardList)) {
        //         //
        //         //         header = RulingListHelper.getRulingHeader(ruling, cardList, index);
        //         //         //console.log(header, headers, text);
        //         //         // if (_.isArray(header)) {
        //         //         //     _.each(header, (name) => {
        //         //         //         if (!headers.includes(name)) {
        //         //         //             text += name;
        //         //         //
        //         //         //             headers.push(name);
        //         //         //         }
        //         //         //     });
        //         //         // }
        //         //         //
        //         //         // console.log(header, text);
        //         //
        //         //         // if ( !headers.includes(header[0]) ) {
        //         //         //     text = header;
        //         //         //     headers.push(header[0])
        //         //         // } else {
        //         //         //     text = '';
        //         //         // }
        //         //
        //         //         return (
        //         //             <RulingItem key={ index }
        //         //                         parent={ self }
        //         //                         ruling={ ruling }
        //         //                         headerText={ text } />
        //         //         );
        //         //     }
        //         //
        //         // });
        //
        //     } catch (err) {
        //         Logger.warn('RulingList renderRulingList problem', err);
        //     }
        // }

    }


}

export default RulingList;
