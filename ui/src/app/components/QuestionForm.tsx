/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-03-12
 * Time: 16:43
 */

import * as React from 'react';
import { Component } from 'react';
import { addEvent, removeEvent, preventEvent } from '../helpers/CustomHandler';
import Button from './Button';

class QuestionForm extends Component {
    constructor (props) {
        super(props);

        this.state = {
            author: '',
            email: '',
            question: '',
            errorMsg: ''
        };

        this.parent = this.props.parent;
    }

    componentDidMount () {
        addEvent(this.refs.emailInput, 'keyup', this.updateEmail.bind(this));
        addEvent(this.refs.questionInput, 'keyup', this.updateQuestion.bind(this));
        addEvent(this.refs.nameInput, 'keyup', this.updateName.bind(this));
        addEvent(this.refs.form, 'submit', this.onSubmit.bind(this));
    }

    componentWillUnmount () {
        removeEvent(this.refs.emailInput, 'keyup', this.updateEmail);
        removeEvent(this.refs.questionInput, 'keyup', this.updateQuestion);
        removeEvent(this.refs.nameInput, 'keyup', this.updateName);
        removeEvent(this.refs.form, 'submit', this.onSubmit);
    }

    render() {

        return (
            <form ref="form" className="SL-form questionForm">
                <label className="form-label questionFormLabel">
                    <strong className="labelText">
                        <i className="fa fa-user-circle" aria-hidden="true" />
                        <span className="placeholder-text">Your name</span>
                    </strong>
                    <input type="text"
                           id="author"
                           className="input"
                           ref="authorInput"
                           placeholder="Your name" />
                </label>
                <label className="form-label questionFormLabel">
                    <strong className="labelText">
                        <i className="fa fa-envelope-o" aria-hidden="true" />
                        <span className="placeholder-text">Your email address</span>
                    </strong>
                    <input type="email"
                           id="email"
                           className="input"
                           ref="emailInput"
                           placeholder="Your email address" />
                </label>

                <label className="form-label questionFormLabel">
                    <strong className="labelText">
                        <span className="placeholder-text">Your question</span>
                    </strong>
                    <textarea type="question"
                              id="question"
                              className="input"
                              placeholder="Your question"
                              ref="questionInput"/>
                </label>

                <label className="form-label clearfix">
                    <Button ref="submitBtn"
                            cssClass="btn btn-primary btn-submit submitRulingBtn"
                            parent={this}>Submit a question</Button>
                </label>
            </form>
        );
    }

    updateName () {
        this.setState({
            author: this.refs.authorInput.value
        });
    }

    updateEmail () {
        this.setState({
            email: this.refs.emailInput.value
        });
    }

    updateQuestion () {
        this.setState({
            question: this.refs.questionInput.value
        });
    }

    onSubmit (ev) {
        preventEvent(ev);

        let { state } = this.props;

        if ( this.isFormValid() ) {
            this.refs.submitBtn.setButtonInLoadingState().disableButton();

        } else {
            this.refs.submitBtn.setButtonInLoadingState();

            alert(this.state.errorMsg.toString());
        }

        let data = {
            author: this.state.author,
            email: this.state.email,
            body: this.state.question,
            firstCard: (state.cardList && typeof state.cardList[0] !== 'undefined') ? state.cardList[0].id : '',
            secondCard: (state.cardList && typeof state.cardList[1] !== 'undefined') ? state.cardList[1].id : '',
            thirdCard: (state.cardList && typeof state.cardList[2] !== 'undefined') ? state.cardList[2].id : ''
        };

        this.parent.onFormSubmit(data);
    }

    onButtonClick (button, ev) {
        button.setButtonInLoadingState();
        this.onSubmit(ev);
    }

    enableButton () {
        this.refs.submitBtn.enableButton().setButtonInReadyState();
    }

    isFormValid () {
        let { state } = this.props;

        if (this.state.email == '' || this.state.question == '' || this.state.author) {

            this.setState({
                errorMsg: 'All fields are required!'
            });

            this.refs.submitBtn.disableButton();

            return false;

        } else if (state.cardList.length < 2) {
            this.setState({
                errorMsg: 'Select minimum 2 cards to submit a question'
            });

            this.refs.submitBtn.disableButton();

            return false;
        } else {
            this.setState({
                errorMsg: ''
            });

            this.refs.submitBtn.setButtonInReadyState();

            return true;
        }
    }
}

export default QuestionForm;
