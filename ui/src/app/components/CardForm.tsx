/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-11
 * Time: 16:25
 */

// @flow

import * as React from 'react';
import { Component, ReactPropTypes, KeyboardEventHandler, InputHTMLAttributes } from 'react';
import { connect, Dispatch } from 'react-redux';
import { getScrollbarWidth } from '../helpers/general';
import Button from './Button';
import Autocompletter from './Autocompletter';
import { getCardData } from '../actions/action-creators';
import Logger from '../helpers/Logger';
import { SuggestionEntity, SuggestionListEntity, StateEntity } from '../entities'
import { CardEntity } from '../entities/card-entities';

export interface InputPropsInterface extends InputHTMLAttributes<any> {
    className: string;
    autoFocus?: boolean;
    placeholder?: string;
    autoCorrect?: string;
    autoComplete?: string;
    value?: undefined | number | string | Array<string>;
    onKeyUp?: KeyboardEventHandler<any>;
    ref?: any;
    type?: string;
};

export interface CardFormProps extends ReactPropTypes {
    parent: any;
    suggestionList: SuggestionListEntity;
    cardList: Array<CardEntity>;
    cid: number;
    dispatch: Dispatch<any>;
}

interface internalState {
    value: string;
    cards: Array<CardEntity>;
    loading: boolean;
    submitDisabled: boolean;
    isVisible: boolean;
    typedCardName: string;
}

class CardForm extends Component<CardFormProps, any> {

    submitBtn: Button;
    autocompletter: any;
    parent: any;
    input: Element;
    t: number;
    state: internalState;

    constructor(props: CardFormProps) {
        super(props);

        this.parent = this.props.parent;

        this.state = {
            value: '',
            cards: [],
            loading: false,
            submitDisabled: true,
            isVisible: false,
            typedCardName: ''
        }
    }

    componentDidUpdate() {

        let root = document.getElementsByTagName('body')[0];

        if (this.state.isVisible) {
            root.className += ' no-scroll';
            document.body.style.cssText = 'padding-right: ' + getScrollbarWidth() + 'px';
        } else {
            root.className = root.className.replace(/no\-scroll/g, '').trim();
            document.body.style.paddingRight = '';
        }
    }

    componentWillUnmount() {

        let root = document.getElementsByTagName('body')[0];
        root.className = root.className.replace(/no\-scroll/g, '').trim();

        document.body.style.paddingRight = '';
    }

    render(): JSX.Element | null {

        return (this.state.isVisible) ? this.renderCardForm() : null;
    }

    renderCardForm (): JSX.Element {

        const suggestionList = this.props.suggestionList;
        interface CardFormInputProps extends InputPropsInterface {

        }
        
        const inputProps: InputPropsInterface = {
            className: "input",
            autoFocus: true,
            placeholder: "Start typing...",
            autoCorrect: "off",
            autoComplete: "off",
            value: suggestionList.selected ? suggestionList.selected.name : undefined,
            ref: (autocompletter: any) => { this.autocompletter = autocompletter }
        };

        return (
            <div className="popup cardFormPopup">
                <div className="popupBox">
                    <button className="btn closeBtn"
                            onClick={ () => this.onClose() }
                    >
                        <i className="fa fa-times" aria-hidden="true"/>
                    </button>
                    <h3 className="popupHeader">Find a card to add to the ruling</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
                    <div className="SL-form cardForm">

                        <label className="form-label cardFormLabel">
                            <strong className="labelText">
                                <span className="placeholder-text">Card name</span>
                            </strong>

                            <Autocompletter {...inputProps} />
                        </label>

                        <label className="cardFormLabel clearfix">
                            <Button ref={ (btn: Button) => { this.submitBtn = btn; }}
                                    parent={ this }
                                    cssClass="btn btn-secondary btn-submit submitCardBtn"
                                    callback={ () => this.onSubmit() }
                            >
                                Add card
                            </Button>
                        </label>
                    </div>

                </div>
                <div className="popupLayer" onClick={ () => this.onClose() } />
            </div>
        );

    }

    onClose(): void {

        try {
            this.setState({ isVisible: false });
        } catch (err) {
            Logger.warn('onClose problem', err);
        }

    }


    /**
     * set isVisible to show or hide popup
     * @param {bool} visibility
     */
    setPopupVisibility(visibility: boolean): CardForm {
        this.setState({ isVisible: visibility });

        return this;
    }

    onSubmit(): void {

        try {

            const dispatch: Dispatch<any> = this.props.dispatch;
            const suggestionList: SuggestionListEntity = this.props.suggestionList;
            const cid: number = this.props.cid;

            this.submitBtn.setButtonInLoadingState();

            if (suggestionList.selected !== null) {
                dispatch( getCardData( suggestionList.selected, cid ) )
            }

        } catch (err) {
            Logger.warn('CardForm onSubmit problem', err);
        }
    }

    onButtonClick(ev: Event): void {
        this.onSubmit();
    }


}

function mapStateToProps (state: StateEntity) {
    return {
        cardList: state.cardList,
        suggestionList: state.suggestionList
    }
}

export default connect(mapStateToProps, null, null, { withRef: true })(CardForm);
