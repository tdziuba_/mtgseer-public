/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-04-02
 * Time: 11:05
 */

import * as React from 'react';
import { Component, ReactPropTypes } from 'react';
import { RulingEntity } from '../entities';

interface Props {
    key: number;
    headerText: string;
    ruling: RulingEntity
}

export default class RulingItem extends Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    render() {

        let { ruling, headerText } = this.props;

        return (
            <div className="rulingItem">

                { this.renderHeader(headerText) }

                <p>{ ruling.text }</p>
                <p className="basedOn"><strong>Based on: { ruling.basedOn }</strong> by { ruling.author }</p>
            </div>
        );
    }

    renderHeader (headerText: string) {

        if (headerText === '') {
            return null;
        }

        return (
            <h3>{ headerText }</h3>
        );

    }
}
