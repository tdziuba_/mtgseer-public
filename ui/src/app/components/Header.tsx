/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2016-12-25
 * Time: 13:13
 */

import * as React from 'react';
import { Component, ReactPropTypes, ReactHTMLElement } from 'react';
import { Link } from 'react-router-dom';
import { StateEntity } from '../entities';

interface LinkAttributes<T> extends JSX.IntrinsicAttributes {
    activeClassName: T;
    className: T;
}

export interface HeaderProps extends JSX.IntrinsicAttributes {
    state: StateEntity;
    parent: any;
}

interface InternalState {
    isMenuOpen: boolean;
}

class Header extends Component<HeaderProps, any> {

    menuBtn: HTMLButtonElement | null;
    state: InternalState;

    links: any;

    constructor(props: HeaderProps) {
        super(props)

        this.state = {
            isMenuOpen: false
        };
    }

    onMenuButtonClick(): void {
        this.links.className = this.getLinksClassName();
    }

    getLinksClassName(): string {
        let cssClass = this.links.className;

        if (cssClass.indexOf('mobile-hide tablet-hide') > -1) {
            return cssClass.replace('mobile-hide tablet-hide', '');
        } else {
            return cssClass + ' mobile-hide tablet-hide';
        }

    }

    render(): JSX.Element {

        const LinkAttributesObject = {
            activeClassName: "active",
            className: "headerLink navLink"
        }

        return (
            <div className="SL-header container">
                <Link to="/" className="logo headerLink">
                    <h1 className="title">
                        <img className="responsive"
                             src="/assets/img/logo.png"
                             srcSet="/assets/img/logo@2x.png 2x, /assets/img/logo@3x.png 2x"
                             alt="MTG Seer"/>
                        <span className="sr-only">MTG Seer</span>
                        <small className="subTitle mobile-sr">  comprehensive MTG Ruling database</small></h1>
                </Link>
                <nav ref={(links) => { this.links = links }}
                     className="headerLinks mobile-hide tablet-hide"
                >
                    <Link to="/about" {...LinkAttributesObject}><span className="navLinkText">About</span></Link>
                    <Link to="/submit-question" {...LinkAttributesObject}><span className="navLinkText">Submit a question</span></Link>
                    <Link to="/new-ruling" {...LinkAttributesObject}><span className="navLinkText">Submit new Ruling</span></Link>
                </nav>
                <button className="btn mobileMenuBtn desktop-hide large-hide"
                        ref={(menuBtn: HTMLButtonElement) => { this.menuBtn = menuBtn }}
                        onClick={this.onMenuButtonClick}
                >
                    <i className="fa fa-bars" aria-hidden="true" />
                </button>
            </div>
        );
    }
}

export default Header;
