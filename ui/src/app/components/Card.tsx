/**
 * Created by Tomasz Dziuba <tomasz.dziuba@poczta.pl>.
 * Date: 2017-01-11
 * Time: 11:05
 */

import * as React from 'react'; 
import { Component, ReactPropTypes, ReactChild, ReactInstance } from 'react';
import { CardForm, CardFormProps } from './CardForm';
import { receiveCard, removeCard } from '../actions/card-actions';
import { receiveRuling, removeRuling } from '../actions/ruling-actions';
import { connect } from 'react-redux';
import axios from 'axios';
import { Logger } from '../helpers';
import { CardEntity, StateEntity } from '../entities';

interface InternalState {
    card: null | CardEntity;
    cid: null | number;
    showPicture: boolean;
};

interface CardProps extends ReactPropTypes {
    cid: number;
    parent: Component;
    cardList: Array<CardEntity>;
    state: StateEntity;
}
class Card extends Component<CardProps, any> {

    parent: Component;
    cardForm: null | ReactInstance;
    state: InternalState;

    constructor (props: CardProps) {
        super(props);

        this.parent = this.props.parent;
        this.cardForm = null;

        this.state = {
            card: null,
            cid: null,
            showPicture: false
        };
    }

    componentDidMount (): void {
        const cardList: Array<CardEntity> = this.props.cardList;
        const cid: number = this.props.cid;

        this.setState({
            card: (typeof cardList[cid] !== 'undefined') ? cardList[cid] : null,
            cid: (typeof cid !== 'undefined') ? cid : null
        });
    }

    getCardState (): StateEntity {
        return this.props.state;
    }

    renderEmptySlot (): JSX.Element {
        const cid: number = this.props.cid;
        const cardForm = this.cardForm as any;

        const formProps = {
            cid: cid
        }

        return (
            <div className={ this.getCardCssClass() }>
                <div className="main">
                    <div className="formWrp"
                         onClick={ () => { cardForm.getWrappedInstance().setPopupVisibility(true) } }>
                        <div className="actionWrp">
                            <button className="btn btn-primary addCardBtn" title="Add card ruling"><i className="fa fa-plus" aria-hidden="true" /></button>
                            <p className="addNewCardText"><strong>Add</strong> {cid > 0 ? 'another': ''} card to ruling</p>
                        </div>
                    </div>
                </div>
                <CardForm ref={ (cardForm: CardForm) => { this.cardForm = cardForm; } }
                          {...formProps}
                />
            </div>
        );
    }

    renderCard (card) {

        return (
            <div className={ this.getCardCssClass() }>
                <div className="main">
                    <picture className="cardPicture">
                        <img className="cardImage" src={card.image} alt={card.name} />
                    </picture>
                    <h3 className="cardTitle">{card.name}</h3>
                    <p className="cardType"><strong>{card.type}</strong></p>
                    <div className="cardDescription">{card.text}</div>
                    <button className="btn btn-default desktop-hide large-hide viewCardBtn"
                            title="Show card details"
                            onClick={ () => this.togglePicture() }
                    >
                        <i className="fa fa-eye" aria-hidden="true" />
                    </button>
                    <button className="btn btn-primary removeCardBtn"
                            title="Remove card from slot"
                            onClick={ () => this.removeCard() }
                    >
                        <i className="fa fa-trash" aria-hidden="true" />
                    </button>
                </div>
            </div>
        );
    }

    render () {
        let { cardList, cid } = this.props;

        let card = (cardList && typeof cardList[cid]) ? cardList[cid] : this.state.card;

        if (card) {
            return this.renderCard(card);
        } else {
            return this.renderEmptySlot();
        }

    }

    getCardCssClass () {
        let cssClass = 'SL-card-slot empty short',
            { cardList, cid } = this.props,
            isFilled = ( this.state.card || (typeof cardList[cid] !== 'undefined' && cardList[cid] !== null) );

        cssClass = (isFilled) ? cssClass.replace(' empty', ' filled').trim() : cssClass.replace(' filled', ' empty').trim();

        cssClass = (!this.state.showPicture) ? cssClass.replace(' full', ' short').trim() : cssClass.replace(' short', ' full').trim();

        return cssClass;

    }

    togglePicture () {
        let showPic = !(this.state.showPicture);

        this.setState({ showPicture: showPic });
    }

    removeCard () {
        let { dispatch, cardList, cid } = this.props;

        try {
            dispatch( removeCard( this.state.card, cid ) );

            this.setState({ card: null, cid: null });
        } catch (err) {
            Logger.warn('removing card problem', err);
        }

        try {
            let rulingIds = cardList[cid].rulings;

            rulingIds.map(id => {
                dispatch( removeRuling( id.replace('/rulings/', '') ) );
            });
        } catch (err) {
            Logger.warn('removing rulings problem', err);
        }

    }

}

export default connect(state => ({
    cardList: state.cardList,
    suggestionList: state.suggestionList
}), null, null, { withRef: true })(Card);
