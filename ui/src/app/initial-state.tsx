import { CardEntity, RulingEntity, SuggestionListEntity, StateEntity } from './entities'


const InitialState: StateEntity = {
    // config: [],
    cardList: new Array(3),
    rulingList: [],
    suggestionList: {
        suggestions: [],
        selected: null
    }
}

export default InitialState;