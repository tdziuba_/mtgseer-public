import * as React from 'react';
import { Component, ReactPropTypes, ErrorInfo, ReactNode } from 'react';
import { connect, ProviderProps } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/ruling-actions';
import Header, { HeaderProps } from '../components/Header';
import Logger from '../helpers/Logger';
import { Dispatch, Action } from 'redux';
import InitialState from '../initial-state'
import { StateEntity } from '../entities'

type State = {
    hasError: boolean;
  };

class App extends Component<any, any> {
    state: State;

    constructor(props: ProviderProps) {
        super(props);

        this.state = {
            hasError:  false
        };
    }

    componentDidCatch(error: Error, info: ErrorInfo) {
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        Logger.warn(info, error);
    }

    render () {
        const children: ReactNode = this.props.children;
        const state: StateEntity = this.props.state;

        const headerProps: HeaderProps = {
            state,
            parent: this,
        }

        return (
            <div className="SL-app">
                <Header {...headerProps} />
                <section className="SL-content">
                    { children }
                </section>
                <footer className="SL-footer"><p>Copyrights &copy; { this.getYear() } by MTG Seer </p></footer>
            </div>
        );
    }

    getYear(): number {
        const date: Date = new Date();

        return date.getFullYear();
    }

}

const mapStateToProps = (state: StateEntity) => ({
    state
});

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
