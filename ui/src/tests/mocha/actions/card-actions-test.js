/**
 * Created by Tomasz Dziuba on 11.05.2017.
 */
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as actions from "../../../app/actions/card-actions";
import * as types from "../../../app/actions/action-constants.tsx";
import { expect, assert } from "chai"; // You can use any testing library

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe("card actions", () => {
    it("describe receiving card action", () => {
        const initialState = {};
        const store = mockStore(initialState);
        const c = {};
        const expectedAction = {
            type: types.RECEIVE_SINGLE_CARD,
            card: c,
            cid: 1,
        };

        store.dispatch(actions.receiveCard(c, 1));

        const acts = store.getActions();
        expect(acts).to.eql([expectedAction]);
    });

    it("describe removing card action", () => {
        const initialState = {};
        const store = mockStore(initialState);

        const c = {};
        const expectedAction = {
            type: types.REMOVE_CARD,
            card: c,
            cid: 1,
        };

        store.dispatch(actions.removeCard(c, 1));

        const acts = store.getActions();
        expect(acts).to.eql([expectedAction]);
    });
});
