import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as actions from "../../../app/actions/suggestion-actions";
import * as types from "../../../app/actions/action-constants.tsx";
import { expect, assert } from "chai"; // You can use any testing library

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe("suggestion actions", () => {
    it("describe receiving autosuggestions action", () => {
        const initialState = {};
        const store = mockStore(initialState);
        const s = [{ id: 1, name: "suggestion1" }, { id: 2, name: "suggestion2" }];
        const expectedAction = {
            type: types.RECEIVE_SUGGESTIONS,
            suggestions: s,
        };

        store.dispatch(actions.receiveAutosugesstions(s));

        const acts = store.getActions();
        expect(acts).to.eql([expectedAction]);
    });

    it("describe clearing autosuggestion list action", () => {
        const initialState = {
            suggestions: [{ id: 1, name: "suggestion1" }, { id: 2, name: "suggestion2" }],
        };
        const store = mockStore(initialState);

        const expectedAction = {
            type: types.CLEAR_SUGGESTIONS,
            suggestions: [],
        };

        store.dispatch(actions.clearAutosugesstions());

        const acts = store.getActions();
        expect(acts).to.eql([expectedAction]);
    });
});
