import MockStorage from 'redux-mock-store';
import jsdom from 'jsdom';


const jsdomDoc = new jsdom.JSDOM('<!doctype html><html><body></body></html>', { url: 'http://localhost:3000' });
let win = jsdomDoc.defaultView;

global.document = jsdomDoc;
global.window = win;

// global.window.localStorage = new MockStorage();
// global.window.sessionStorage = new MockStorage();

propagateToGlobal(win);

// global.window = jsdomDoc.defaultView;
// global.document = window.document;
// global.navigator = window.navigator;
// global.XMLHttpRequest = window.XMLHttpRequest;
// global.HTMLElement = window.HTMLElement;




// from mocha-jsdom https://github.com/rstacruz/mocha-jsdom/blob/master/index.js#L80
function propagateToGlobal (window) {
    for (let key in window) {
        if (!window.hasOwnProperty(key)) continue
        if (key in global) continue

        global[key] = window[key]
    }
}

