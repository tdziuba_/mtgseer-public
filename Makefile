.PHONY: help
help:
	@echo "For local usage only."
	@echo "  setup        downloads dependencies"
	@echo "  clean        Remove dependencies"

.PHONY: help
setup: bin/kubectl

platform := $(shell uname -a|cut -d ' ' -f 1| tr [:upper:] [:lower:])
bin/kubectl:
	curl -o bin/kubectl -LO https://storage.googleapis.com/kubernetes-release/release/$(shell curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/${platform}/amd64/kubectl
	chmod +x bin/kubectl

.PHONY: clean
clean:
	rm -f bin/kubctl
