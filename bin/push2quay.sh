#!/bin/bash
source build.env
docker login -e="." -u=$DOCKER_USER -p=$DOCKER_PASS quay.io

# Only push artifacts that needed 'build'-ing
cat $CIRCLE_ARTIFACTS/build | while read artifact
do
    docker-compose push  --ignore-push-failures ${artifact} 2>&1
done

