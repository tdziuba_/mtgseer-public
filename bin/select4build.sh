#!/bin/bash
source build.env
APPS=`for app in \`ls */Dockerfile*\` ; do dirname $app ; done | sort | uniq`

echo "Current build hash : ${CIRCLE_SHA1}"

for app in $APPS
do
	SHA1_HASH=`git log --stat -n 1 --follow ${app} | grep commit | awk '{print $2}'`
	ENV_APP="`echo $app|  awk '{print toupper($0)}'`_SHA1"
	echo "export $ENV_APP=$SHA1_HASH" >> build.env
	
	list=`curl -s -u ${DOCKER_USER}:${DOCKER_PASS} https://quay.io/v1/repositories/${QUAY_ORG}/${app}/tags`

	if [ `echo $list | grep ${SHA1_HASH}` ]; then
		echo "Current image of ${app}:${SHA1_HASH} exist in Quay.io, build is not needed"
		echo "Image to be deployed: ${app}:${SHA1_HASH}"
		echo "${app}=${SHA1_HASH}" >> $CIRCLE_ARTIFACTS/deploy
	else
		echo "Image to be built: ${app}:${SHA1_HASH}"
		echo ${app} >> $CIRCLE_ARTIFACTS/build
		echo "Image to be deployed: ${app}:${SHA1_HASH}"
		echo "${app}=${SHA1_HASH}" >> $CIRCLE_ARTIFACTS/deploy
	fi
done
