#!/bin/bash -x
exit 0
echo "Running tests on changed artifacts"
cat $CIRCLE_ARTIFACTS/build | while read artifact
do
    docker-compose run --no-deps ${artifact} ./run_ci.sh
done
