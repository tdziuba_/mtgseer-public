#!/bin/bash
# Requires NAMESPACE and CIRCLE_SHA1(for populate_etcd.sh) to be defined.
. `dirname $0`/setup_k8s.sh

# Insert Quay Docker Build user credentials
${kubecommand} apply -f ./bin/deployment/k8s/mtgseer-deployer-secret.yml

# Deploy all the services (etc, databases)
ls ./bin/deployment/k8s/services/*.yml | while read service
do
    echo "Deploying Service : $service"
    ${kubecommand} apply -f ${service}
done

#ToDo remove this sleep with a liveness check
sleep 10

# Deploy Configuration
./bin/deployment/k8s/bin/populate_etcd.sh k8s.env
