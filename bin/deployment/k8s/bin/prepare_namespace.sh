#!/bin/bash
# Requires NAMESPACE to be defined.
. `dirname $0`/setup_k8s.sh

echo "Creating the namespace: ${NAMESPACE}"

#echo "root"
#ls
#echo "k8s"
#ls ./bin/deployment/k8s

envsubst '${NAMESPACE}' < ./bin/deployment/k8s/namespace.template > ./bin/deployment/k8s/tmp/namespace.yml

${kubectl} apply -f ./bin/deployment/k8s/tmp/namespace.yml
