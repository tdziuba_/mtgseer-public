#!/bin/bash
# Requires NAMESPACE to be defined.
export kubectl="./bin/kubectl --kubeconfig=bin/deployment/k8s/config/kube-config"
export kubecommand="$kubectl --namespace=${NAMESPACE}"
