#!/bin/bash
# Requires $CONFIG_VERSION and $NAMESPACE to be defined.
. `dirname $0`/setup_k8s.sh

#Load the SHA values for deployment
source $CIRCLE_ARTIFACTS/deploy

ls ./bin/deployment/k8s/deployments/*.template | while read def
do
	artifact=$(basename ${def%.template})

    #Set the SHA value as set by the select4Build script.
    export TAG=${!artifact}

	envsubst '${TAG} ${CONFIG_VERSION} ${NAMESPACE}' < $def > bin/deployment/k8s/tmp/${artifact}.yml

	echo "Launching ${artifact}:${TAG} with Config:${CONFIG_VERSION} in ${NAMESPACE}"
    ${kubecommand} apply -f ./bin/deployment/k8s/tmp/${artifact}.yml
done
