#!/bin/bash
# Requires NAMESPACE to be defined.
. `dirname $0`/setup_k8s.sh

# Echo out deployments and pods
${kubecommand} get deployments
${kubecommand} get pods
${kubecommand} get services
