#!/bin/bash -x

export NAMESPACE=${CIRCLE_BRANCH}
export CONFIG_VERSION=${CIRCLE_SHA1}

mkdir -p bin/deployment/k8s/tmp
`dirname $0`/setup_k8s.sh
`dirname $0`/prepare_namespace.sh
`dirname $0`/deploy_services.sh
`dirname $0`/deploy_apps.sh
`dirname $0`/check_status.sh
