#!/bin/bash -x
# Requires NAMESPACE, CIRCLE_SHA1 and CONFIG_VERSION to be defined.
. `dirname $0`/setup_k8s.sh

setValueInETCD () {
  variable=`echo $1 | cut -d '=' -f 1`
  value=`echo $1 | cut -d '=' -f 2-`
  ${kubecommand} exec etcd0 -ti -- /usr/local/bin/etcdctl set /${CONFIG_VERSION}/${variable} ${value} &
}

echo "Copying Environment to etcd"
source $1
for var in `grep -v ^# $1 | cut -d '=' -f 1 | xargs echo` ; do
    `set -o posix ; set | grep ^${var} | xargs echo setValueInETCD`
done
