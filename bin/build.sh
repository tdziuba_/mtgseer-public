#!/bin/bash
source build.env
echo "Running build on changed artifacts"
cat $CIRCLE_ARTIFACTS/build | while read artifact
do
    docker-compose build ${artifact}
done
